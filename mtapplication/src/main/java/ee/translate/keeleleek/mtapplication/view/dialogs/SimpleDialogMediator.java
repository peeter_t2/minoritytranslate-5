package ee.translate.keeleleek.mtapplication.view.dialogs;

import java.util.List;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.common.LazyCaller;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.suggestions.SearchRequest;
import ee.translate.keeleleek.mtapplication.model.suggestions.SearchRequest.RequestType;
import ee.translate.keeleleek.mtapplication.model.suggestions.SearchResponse;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;


public abstract class SimpleDialogMediator extends Mediator implements ConfirmDeclineDialog {

	final public static String NAME_ADD_ARTICLE = "{DECC71DF-0944-4DE2-BD91-15D9551DA0E0}";
	final public static String NAME_ADD_CATEGORY = "{CF4623FA-95FE-46F4-B55E-7F9A4D4B74A2}";
	final public static String NAME_REMOVE_ARTICLE = "{DA68D647-1464-4B98-9B89-5DA1A7B11402}";
	final public static String NAME_ADD_LINKS = "{0D3A0919-311A-43FE-8F64-33EC62F8864D}";
	
	public final static long TITLE_DELAY = 200;
	
	protected String nameLabel;
	protected String titleCommand;
	protected String languageCommand;
	protected String confirmCommand;
	protected String openCommand;
	protected RequestType type;
	
	private LazyCaller caller;
	
	protected boolean feedback = false;
	
	
	// INIT
	public SimpleDialogMediator(String name) {
		super(name, null);
	}

	@Override
	public void onRegister()
	 {
		feedback = true;
		
		super.onRegister();

		switch (getMediatorName()) {
		case NAME_ADD_CATEGORY:
			nameLabel = Messages.getString("sdialog.add.category.title");
			titleCommand = Notifications.SUGGEST_CATEGORY;
			languageCommand = Notifications.SUGGEST_LANGUAGE;
			confirmCommand = Notifications.MENU_ADD_CATEGORY_CONFIRMED;
			openCommand = Notifications.WINDOW_ADD_CATEGORY_OPEN;
			type = RequestType.CATEGORIES;
			initHeadingLabel(Messages.getString("sdialog.add.category.header"));
			initNameLabel(nameLabel);
			break;
			
		case NAME_ADD_ARTICLE:
			nameLabel = Messages.getString("sdialog.add.article.title");
			titleCommand = Notifications.SUGGEST_TITLE;
			languageCommand = Notifications.SUGGEST_LANGUAGE;
			confirmCommand = Notifications.MENU_ADD_ARTICLE_CONFIRMED;
			openCommand = Notifications.WINDOW_ADD_ARTICLE_OPEN;
			type = RequestType.ARTICLES;
			initHeadingLabel(Messages.getString("sdialog.add.article.header"));
			initNameLabel(nameLabel);
			break;
			
		case NAME_REMOVE_ARTICLE:
			nameLabel = Messages.getString("sdialog.remove.article.title");
			titleCommand = Notifications.SUGGEST_LOADED_TITLE;
			languageCommand = Notifications.SUGGEST_LOADED_LANGUAGE;
			confirmCommand = Notifications.MENU_REMOVE_ARTICLE_CONFIRMED;
			openCommand = Notifications.WINDOW_REMOVE_ARTICLE_OPEN;
			type = RequestType.LOADED_ARTICLES;
			initHeadingLabel(Messages.getString("sdialog.remove.article.header"));
			initNameLabel(nameLabel);
			break;

		case NAME_ADD_LINKS:
			nameLabel = Messages.getString("sdialog.add.links.title");
			titleCommand = Notifications.SUGGEST_TITLE;
			languageCommand = Notifications.SUGGEST_LANGUAGE;
			confirmCommand = Notifications.MENU_ADD_LINKS_CONFIRMED;
			openCommand = Notifications.WINDOW_ADD_LINKS_OPEN;
			type = RequestType.ARTICLE_LINKS;
			initHeadingLabel(Messages.getString("sdialog.add.links.header"));
			initNameLabel(nameLabel);
			break;
			
		default:
			break;
		}
		
		caller = new LazyCaller(new Runnable() {
			@Override
			public void run()
			 {
				String langCode = getLangCode();
				String title = getName();
				
				if (langCode == null) return;
				
				SearchRequest request = new SearchRequest(langCode, type, title);
				sendNotification(Notifications.REQUEST_SUGGESTIONS, request);
			 }
		}, TITLE_DELAY);
		
		feedback = false;
	 }
	
	
	// NOTIFICATIONS
	@Override
	public String[] listNotificationInterests() {
		return new String[]{
			Notifications.SESSION_LOADED,
			Notifications.PREFERENCES_LANGUAGES_CHANGED,
			Notifications.SUGGESTIONS_CHANGED,
			Notifications.WINDOW_ADD_CATEGORY_OPEN,
			Notifications.WINDOW_ADD_ARTICLE_OPEN,
			Notifications.WINDOW_REMOVE_ARTICLE_OPEN,
			Notifications.WINDOW_ADD_LINKS_OPEN
		};
	}
	
	@Override
	public void handleNotification(INotification notification)
	 {
		feedback = true;
		
		SearchResponse response;
		
		switch (notification.getName()) {
		case Notifications.SESSION_LOADED:
		case Notifications.PREFERENCES_LANGUAGES_CHANGED:
			List<String> langCodes =  MinorityTranslateModel.preferences().languages().getAllLangCodes();
			List<String> langNames = MinorityTranslateModel.wikis().getLangNames(langCodes);
			initLanguages(langNames);
			break;

		case Notifications.SUGGESTIONS_CHANGED:
			response = (SearchResponse) notification.getBody();
			if (response.getRequest().getType() == this.type) suggestTitles(response.getResults());
			break;

		case Notifications.WINDOW_ADD_CATEGORY_OPEN:
		case Notifications.WINDOW_ADD_ARTICLE_OPEN:
		case Notifications.WINDOW_REMOVE_ARTICLE_OPEN:
		case Notifications.WINDOW_ADD_LINKS_OPEN:
			if (!notification.getName().endsWith(openCommand)) break;
			startEditing();
			break;
			
		default:
			break;
		}
		
		feedback = false;
	 }

	
	// VALUES
	/**
	 * Gets language code.
	 * 
	 * @return language code, null if none
	 */
	public String getLangCode() {
		String langName = getLangName();
		return MinorityTranslateModel.wikis().getLangCode(langName);
	}
	
	
	// INHERIT
	protected abstract void initHeadingLabel(String label);
	protected abstract void initNameLabel(String label);
	protected abstract void initLanguages(List<String> langName);
	
	protected abstract String getName();
	protected abstract String getLangName();
	
	protected abstract void updateTitle(String title);
	protected abstract void suggestTitles(String[] results);
	
	protected abstract void startEditing();
	
	
	// INHERIT (CONFIRM CANCEL DIALOG)
	@Override
	public void prepare()
	 {
		
	 }
	
	@Override
	public void focus()
	 {
		startEditing();
	 }
	
	public void confirm()
	 {
		String langCode = getLangCode();
		String name = getName().trim();
   	
		if (langCode == null) {
			DialogFactory.dialogs().showError(Messages.getString("messages.invalid.language.header"), Messages.getString("messages.invalid.language.content.language.not.valid").replace("#", getLangName()));
	   		return;
	   	}
	   	if (name.length() == 0) {
	   		DialogFactory.dialogs().showError(Messages.getString("messages.invalid.name.header"), Messages.getString("messages.invalid.name.content.name.empty"));
	   		return;
	   	}
	   	
	   	sendNotification(Notifications.SESSION_SUGGEST_NAME, name);
	   	
	   	updateTitle("");
	   	
	   	sendNotification(confirmCommand, name, langCode);
	 }
	
	public void decline() {
	   
	}
	
	// EVENTS
    protected void onTitleChanged(String title) {
    	if (feedback) return;
    	caller.call();
	}

    protected void onTitleSelected(String title) {
    	feedback = true;
    	updateTitle(title);
    	feedback = false;
	}

    
}
