package ee.translate.keeleleek.mtapplication.view.dialogs;

import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.MinorityTranslate;
import ee.translate.keeleleek.mtapplication.view.application.ApplicationMediator;


public abstract class AboutDialogMediator extends Mediator {

	public final static String NAME = "{4A1FA6ED-FCBF-4904-811E-22064FD1B752}";
	
	
	// INIT:
	public AboutDialogMediator() {
		super(NAME, null);
	}

	@Override
	public void onRegister() {
		setVersion(MinorityTranslate.VERSION, MinorityTranslate.BUILD_TIME);
	}
	
	
	// IMPLEMENTATION:
	protected abstract void setVersion(String version, String date);
	
	
	// BUTTONS:
	public void onLicenceHyperlinkClick() {
		ApplicationMediator mediator = (ApplicationMediator) getFacade().retrieveMediator(ApplicationMediator.NAME);
		mediator.showDocument("http://www.gnu.org/licenses/gpl-3.0.en.html");
	}

	public void onWMFImageViewClick() {
		ApplicationMediator mediator = (ApplicationMediator) getFacade().retrieveMediator(ApplicationMediator.NAME);
		mediator.showDocument("http://wikimediafoundation.org");
	}

	public void onKeeleleekImageViewClick() {
		ApplicationMediator mediator = (ApplicationMediator) getFacade().retrieveMediator(ApplicationMediator.NAME);
		mediator.showDocument("http://keeleleek.ee");
	}
	
}
