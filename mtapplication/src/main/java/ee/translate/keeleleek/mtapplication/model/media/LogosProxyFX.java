package ee.translate.keeleleek.mtapplication.model.media;

import javafx.scene.image.Image;

public class LogosProxyFX extends LogosProxy {

	public final static String NAME = "{EE1EACEC-FFEA-4C1B-91DE-778E3CE133C9}";
	
	
	@Override
	public Object retrieveApplicationLogo() {
		return new Image("logo.png");
	}

	@Override
	public Image retrieveKeeleleekLogo() {
		return new Image("keeleleek.png");
	}
	
	@Override
	public Image retrieveWMFLogo() {
		return new Image("WMF.png");
	}
	
}
