package ee.translate.keeleleek.mtapplication.view.javafx.application;

import java.awt.Desktop;
import java.io.File;
import java.net.URI;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.keys.KeyHandlerProxy.KeyShortcut;
import ee.translate.keeleleek.mtapplication.view.application.ApplicationMediator;
import ee.translate.keeleleek.mtapplication.view.dialogs.MessageDialogMediator;
import ee.translate.keeleleek.mtapplication.view.javafx.MediatorLoaderFX;
import ee.translate.keeleleek.mtapplication.view.javafx.dialogs.ApplicationDialogFactoryFX;
import ee.translate.keeleleek.mtapplication.view.javafx.dialogs.DialogFactoryFX;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;
import ee.translate.keeleleek.mtapplication.view.windows.TranslateWindowMediator;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class ApplicationMediatorFX extends ApplicationMediator {

	private Image icon;
	
	private Mediator translateWindowMediator;
	private Mediator addArticleMediator;
	private Mediator addCategoryMediator;
	private Mediator addLinksMediator;
	private Mediator removeArticleMediator;
	private Mediator loginMediator;
	private Mediator aboutMediator;
	private Mediator manualMediator;
	
	private Stage transtaleWindow;
	
	private FileChooser sessionChooser = new FileChooser();

	private HashMap<KeyCombination, KeyShortcut> keyMap = new HashMap<>();
	
	
	// INIT
	public ApplicationMediatorFX(Object translateWindow)
	 {
		super();
		
		this.transtaleWindow = (Stage) translateWindow;
		
		DialogFactoryFX.setup(this.transtaleWindow);
		
		// File chooser:
		ExtensionFilter extFilter = new ExtensionFilter(Messages.getString("session.dialog.session.file"), "*.ses");
		sessionChooser.getExtensionFilters().add(extFilter);
	 }
	
	@Override
	public void onRegister()
	 {
		super.onRegister();
		
		transtaleWindow.setTitle(Messages.getString("window.translate"));
		icon = new Image("icon.png");
		transtaleWindow.getIcons().add(icon);
		
		// Translate window:
		translateWindowMediator = (TranslateWindowMediator) MediatorLoaderFX.loadTranslateWindowMediator();
		getFacade().registerMediator(translateWindowMediator);
		transtaleWindow.setScene(new Scene((Parent) translateWindowMediator.getViewComponent()));
		transtaleWindow.getScene().getStylesheets().add(getClass().getResource("/fxview/minoritytranslate.css").toExternalForm());
		transtaleWindow.getScene().getStylesheets().add(getClass().getResource(findFontSizeCSS(MinorityTranslateModel.preferences().getFontSize())).toExternalForm());

		// login mediator
		loginMediator = MediatorLoaderFX.loadLoginWindowMediator();
		getFacade().registerMediator(loginMediator);

		// Add category mediator
		addCategoryMediator = MediatorLoaderFX.loadAddCategoryWindowMediator();
		getFacade().registerMediator(addCategoryMediator);

		// add article mediator
		addArticleMediator = MediatorLoaderFX.loadAddArticleWindowMediator();
		getFacade().registerMediator(addArticleMediator);

		// add links mediator
		addLinksMediator = MediatorLoaderFX.loadAddLinksWindowMediator();
		getFacade().registerMediator(addLinksMediator);

		// remove article mediator
		removeArticleMediator = MediatorLoaderFX.loadRemoveArticleWindowMediator();
		getFacade().registerMediator(removeArticleMediator);

		// about mediator
		aboutMediator = MediatorLoaderFX.loadAboutWindowMediator();
		getFacade().registerMediator(aboutMediator);

		// manual mediator
		manualMediator = MediatorLoaderFX.loadManualWindowMediator();
		getFacade().registerMediator(manualMediator);

		// lists mediator
		
		// key combinations
		keyMap.put(new KeyCodeCombination(KeyCode.DIGIT1, KeyCombination.CONTROL_DOWN), KeyShortcut.SNIPPET_1);
		keyMap.put(new KeyCodeCombination(KeyCode.DIGIT2, KeyCombination.CONTROL_DOWN), KeyShortcut.SNIPPET_2);
		keyMap.put(new KeyCodeCombination(KeyCode.DIGIT3, KeyCombination.CONTROL_DOWN), KeyShortcut.SNIPPET_3);
		keyMap.put(new KeyCodeCombination(KeyCode.DIGIT4, KeyCombination.CONTROL_DOWN), KeyShortcut.SNIPPET_4);
		keyMap.put(new KeyCodeCombination(KeyCode.DIGIT5, KeyCombination.CONTROL_DOWN), KeyShortcut.SNIPPET_5);
		keyMap.put(new KeyCodeCombination(KeyCode.DIGIT6, KeyCombination.CONTROL_DOWN), KeyShortcut.SNIPPET_6);
		keyMap.put(new KeyCodeCombination(KeyCode.DIGIT7, KeyCombination.CONTROL_DOWN), KeyShortcut.SNIPPET_7);
		keyMap.put(new KeyCodeCombination(KeyCode.DIGIT8, KeyCombination.CONTROL_DOWN), KeyShortcut.SNIPPET_8);
		keyMap.put(new KeyCodeCombination(KeyCode.DIGIT9, KeyCombination.CONTROL_DOWN), KeyShortcut.SNIPPET_9);
		
		transtaleWindow.getScene().setOnKeyPressed(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent event)
			 {
				if (!event.isAltDown() && !event.isControlDown()) return;
				Set<Entry<KeyCombination, KeyShortcut>> entries = keyMap.entrySet();
				for (Entry<KeyCombination, KeyShortcut> entry : entries) {
					if (entry.getKey().match(event)) {
						MinorityTranslateModel.keys().handle(entry.getValue());
						event.consume();
						break;
					}
				}
			 }
		});
		
		// Quit request:
		transtaleWindow.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				event.consume();
				sendNotification(Notifications.REQUEST_QUIT);
			}
		});
	 }
	
	private void setupModalWindow(Stage stage, String title)
	 {
		stage.setTitle(title);
		stage.getIcons().add(icon);
		stage.setResizable(false);
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.initOwner(transtaleWindow);
	 }


	// IMPLEMENTATION:
	@Override
	public Object getWindow() {
		return transtaleWindow;
	}
	
	@Override
	protected void setTitle(String title) {
		transtaleWindow.setTitle(title);
	}

	@Override
	protected void translateWindowOpen() {
		transtaleWindow.show();
	}

	@Override
	protected void translateWindowClose() {
		transtaleWindow.close();
	}

	@Override
	protected void loginWindowOpen() {
		ApplicationDialogFactoryFX factory = (ApplicationDialogFactoryFX) DialogFactoryFX.dialogs();
		factory.showConfirmDeclineDialog(loginMediator, Messages.getString("window.login"), Messages.getString("login.header"), Messages.getString("button.ok"), Messages.getString("button.cancel"));
	}

	@Override
	protected void preferencesDialogOpen()
	 {
		Mediator mediator = MediatorLoaderFX.loadPreferencesWindowMediator();
		getFacade().registerMediator(mediator);
		
		ApplicationDialogFactoryFX factory = (ApplicationDialogFactoryFX) DialogFactoryFX.dialogs();
		factory.showConfirmDeclineDialog(mediator, Messages.getString("window.preferences"), null, Messages.getString("button.ok"), Messages.getString("button.cancel"));
		
		getFacade().removeMediator(mediator.getMediatorName());
	 }

	@Override
	protected void addArticleWindowOpen() {
		ApplicationDialogFactoryFX factory = (ApplicationDialogFactoryFX) DialogFactoryFX.dialogs();
		factory.showConfirmDeclineDialog(addArticleMediator, Messages.getString("window.add.article"), Messages.getString("sdialog.add.article.header"), Messages.getString("button.ok"), Messages.getString("button.cancel"));
	}

	@Override
	protected void addCategoryWindowOpen() {
		ApplicationDialogFactoryFX factory = (ApplicationDialogFactoryFX) DialogFactoryFX.dialogs();
		factory.showConfirmDeclineDialog(addCategoryMediator, Messages.getString("window.add.category"), Messages.getString("sdialog.add.category.header"), Messages.getString("button.ok"), Messages.getString("button.cancel"));
	}

	@Override
	protected void addLinksWindowOpen() {
		ApplicationDialogFactoryFX factory = (ApplicationDialogFactoryFX) DialogFactoryFX.dialogs();
		factory.showConfirmDeclineDialog(addLinksMediator, Messages.getString("window.add.links"), Messages.getString("sdialog.add.links.header"), Messages.getString("button.ok"), Messages.getString("button.cancel"));
	}

	@Override
	protected void removeArticleWindowOpen() {
		ApplicationDialogFactoryFX factory = (ApplicationDialogFactoryFX) DialogFactoryFX.dialogs();
		factory.showConfirmDeclineDialog(removeArticleMediator, Messages.getString("window.remove.article"), Messages.getString("sdialog.remove.article.header"), Messages.getString("button.ok"), Messages.getString("button.cancel"));
	}

	@Override
	protected void aboutWindowOpen() {
		ApplicationDialogFactoryFX factory = (ApplicationDialogFactoryFX) DialogFactoryFX.dialogs();
		factory.showCloseDialog(aboutMediator, Messages.getString("window.about"), null, Messages.getString("button.ok"));
	}

	@Override
	protected void manualWindowOpen() {
		ApplicationDialogFactoryFX factory = (ApplicationDialogFactoryFX) DialogFactoryFX.dialogs();
		factory.showCloseDialog(manualMediator, Messages.getString("window.manual"), null, Messages.getString("button.ok"));
	}

	@Override
	protected void sessionOpenWindowOpen() {
		Path initial = MinorityTranslateModel.session().getWorkingPath();
		sessionChooser.setInitialDirectory(initial.toFile());
		File file = sessionChooser.showOpenDialog(transtaleWindow);
		if (file != null) onSessionOpenConfirmed(file.toPath());
	}

	@Override
	protected void sessionSaveWindowOpen() {
		Path initial = MinorityTranslateModel.session().getWorkingPath();
		sessionChooser.setInitialDirectory(initial.toFile());
		sessionChooser.setInitialFileName(MinorityTranslateModel.session().getName());
		File file = sessionChooser.showSaveDialog(transtaleWindow);
		if (file != null) {
			// TODO: Handle workaround:
			if(!file.getName().endsWith(".ses")) file = new File(file.getAbsolutePath() + ".ses");
			onSessionSaveConfirmed(file.toPath());
		}
	}

	@Override
	protected void listsWindowOpen()
	 {
		Mediator listsMediator = MediatorLoaderFX.loadListsWindowMediator();
		
		getFacade().registerMediator(listsMediator);
		
		ApplicationDialogFactoryFX factory = (ApplicationDialogFactoryFX) DialogFactoryFX.dialogs();
		factory.showConfirmDeclineDialog(listsMediator, Messages.getString("window.lists"), null, Messages.getString("button.ok"), Messages.getString("button.cancel"));
		
		getFacade().removeMediator(listsMediator.getMediatorName());
	 }
	
	
	@Override
	protected void showMessage(String title, String message, String details)
	 {
		MessageDialogMediator mediator = MediatorLoaderFX.loadMessageWindowMediator();
		Stage stage = new Stage();
		mediator.setText(message, details);
		stage.setScene(new Scene((Parent) mediator.getViewComponent()));
		
		setupModalWindow(stage, title);
		
		stage.show();
	 }
	
	@Override
	public void showDocument(String uri) {
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					Desktop.getDesktop().browse(new URI(uri));
				} catch (Exception e) {
					LOGGER.warn("Failed to open URI", e);
				}
			}
		});
		thread.start();
	}
	
	
	@Override
	protected void exit() {
		Platform.exit();
	}
	
	
	// HELPERS
	public static String findFontSizeCSS(String name)
	 {
		switch (name) {
		case "100%": return "/css/font/size/100.css";
		case "110%": return "/css/font/size/110.css";
		case "120%": return "/css/font/size/120.css";
		case "150%": return "/css/font/size/150.css";
		case "140%": return "/css/font/size/140.css";
		case "130%": return "/css/font/size/130.css";
		case "90%": return "/css/font/size/90.css";
		case "80%": return "/css/font/size/80.css";
		case "70%": return "/css/font/size/70.css";
		case "60%": return "/css/font/size/60.css";
		case "50%": return "/css/font/size/50.css";
		default: return "/css/font/size/100.css";
		}
	 }
	
	
}
