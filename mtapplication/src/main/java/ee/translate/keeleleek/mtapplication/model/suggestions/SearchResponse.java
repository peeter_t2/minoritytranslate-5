package ee.translate.keeleleek.mtapplication.model.suggestions;

public class SearchResponse {

	private SearchRequest request;
	private String[] results;
	
	
	public SearchResponse(SearchRequest request, String[] suggestions) {
		super();
		this.request = request;
		this.results = suggestions;
	}


	public SearchRequest getRequest() {
		return request;
	}


	public String[] getResults() {
		return results;
	}
	
	
}
