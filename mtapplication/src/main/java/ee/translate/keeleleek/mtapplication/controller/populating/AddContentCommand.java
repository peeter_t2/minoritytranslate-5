package ee.translate.keeleleek.mtapplication.controller.populating;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.common.requests.ContentRequest;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;

public class AddContentCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		ContentRequest request = (ContentRequest) notification.getBody();
		MinorityTranslateModel.queuer().requestContent(request);
	 }
	
}
