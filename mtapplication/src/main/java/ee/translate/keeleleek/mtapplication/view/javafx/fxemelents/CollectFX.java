package ee.translate.keeleleek.mtapplication.view.javafx.fxemelents;

import ee.translate.keeleleek.mtapplication.model.preferences.CollectEntry;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class CollectFX {

	public final StringProperty srcLang;
	public final StringProperty dstLang;
	public final StringProperty find;
	
	
	public CollectFX(CollectEntry entry)
	 {
		this.srcLang = new SimpleStringProperty(entry.getSrcLang());
		this.dstLang = new SimpleStringProperty(entry.getDstLang());
		this.find = new SimpleStringProperty(entry.getFind());
	 }

	public CollectEntry toEntry()
	 {
		return new CollectEntry(srcLang.getValue(), dstLang.getValue(), find.getValue());
	 }
	
	
}
