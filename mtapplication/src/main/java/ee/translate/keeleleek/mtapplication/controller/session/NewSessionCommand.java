package ee.translate.keeleleek.mtapplication.controller.session;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.session.SessionProxy;

public class NewSessionCommand extends SimpleCommand {
	
	@Override
	public void execute(INotification notification)
	 {
		MinorityTranslateModel.lists().cancel();
		MinorityTranslateModel.lists().reset();
		
		if (MinorityTranslateModel.content() != null) MinorityTranslateModel.content().cancel();
		
		SessionProxy sessionProxy = new SessionProxy();
		
		sessionProxy.setSessionPath(null);
		getFacade().registerProxy(sessionProxy);
	 }
	
	
}
