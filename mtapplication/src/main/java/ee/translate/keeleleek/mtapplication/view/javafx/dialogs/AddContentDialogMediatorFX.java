package ee.translate.keeleleek.mtapplication.view.javafx.dialogs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.common.requests.ContentRequest;
import ee.translate.keeleleek.mtapplication.common.requests.ContentSubRequest;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.Namespace;
import ee.translate.keeleleek.mtapplication.model.wikis.WikisProxy;
import ee.translate.keeleleek.mtapplication.view.dialogs.AddContentDialogMediator;
import ee.translate.keeleleek.mtapplication.view.javafx.fxemelents.ContentRequestFX;
import ee.translate.keeleleek.mtapplication.view.javafx.fxemelents.ExposableConfirmFX;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.Clipboard;
import javafx.util.Callback;


public class AddContentDialogMediatorFX extends AddContentDialogMediator implements ExposableConfirmFX {

	private static Logger LOGGER = LoggerFactory.getLogger(WikisProxy.class);

	@FXML
	private TableView<ContentRequestFX> contentTable;
	@FXML
	private TableColumn<ContentRequestFX, String> titleColumn;
	@FXML
	private TableColumn<ContentRequestFX, String> languageColumn;
	@FXML
	private TableColumn<ContentRequestFX, String> namespaceColumn;

	@FXML
	private TextField titleEdit;
	@FXML
	private ComboBox<String> languageSelect;
	@FXML
	private ComboBox<String> namespaceSelect;

	@FXML
	private Button addButton;
	@FXML
	private Button removeButton;
	@FXML
	private Button pasteButton;

	@FXML
	private CheckBox expandCategoriesCheck;
	@FXML
	private CheckBox addArticlesCheck;
	@FXML
	private CheckBox addCategoriesCheck;
	
	
	// INIT
	@FXML
	private void initialize()
	 {
		// languages
		List<String> langCodes = MinorityTranslateModel.preferences().languages().getAllLangCodes();
		for (String langCode : langCodes) {
			String language = MinorityTranslateModel.wikis().getLangName(langCode);
			if (language == null) language = langCode;
			languageSelect.getItems().add(language);
		}
		
		// namespaces
		HashMap<Namespace, String> mappings = MinorityTranslateModel.wikis().findNamespaceMappings();
		Namespace[] namespaces = Namespace.values();
		for (Namespace namespace : namespaces) {
			namespaceSelect.getItems().add(mappings.get(namespace));
		}
		
		// columns
		titleColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ContentRequestFX,String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(CellDataFeatures<ContentRequestFX, String> request) {
				return request.getValue().title();
			}
		});

		languageColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ContentRequestFX,String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(CellDataFeatures<ContentRequestFX, String> request) {
				return request.getValue().language();
			}
		});

		namespaceColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ContentRequestFX,String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(CellDataFeatures<ContentRequestFX, String> request) {
				return request.getValue().namespaceStr();
			}
		});
		
		contentTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<ContentRequestFX>() {
			@Override
			public void changed(ObservableValue<? extends ContentRequestFX> obs, ContentRequestFX oldVal, ContentRequestFX newVal)
			 {
				String oldLanguage = languageSelect.getSelectionModel().getSelectedItem();
				if (oldLanguage == null) oldLanguage = "";
				
				if (oldVal != null) {
					oldVal.title().unbind();
					oldVal.language().unbind();
					oldVal.namespace().unbind();
				}
				if (newVal != null) {
					
					titleEdit.setText(newVal.title().getValue());
					newVal.title().bind(titleEdit.textProperty());

					languageSelect.getSelectionModel().select(newVal.language().getValue());
					newVal.language().bind(languageSelect.getSelectionModel().selectedItemProperty());
					
					if (languageSelect.getSelectionModel().getSelectedItem().isEmpty()) {
						if (oldLanguage.isEmpty()) languageSelect.getSelectionModel().selectFirst();
						else languageSelect.getSelectionModel().select(oldLanguage);
					}

					namespaceSelect.getSelectionModel().select(newVal.namespace().getValue());
					newVal.namespace().bind(namespaceSelect.getSelectionModel().selectedIndexProperty());
					
					titleEdit.setDisable(false);
					languageSelect.setDisable(false);
					namespaceSelect.setDisable(false);
					removeButton.setDisable(false);
					
				} else {
					
					titleEdit.setDisable(true);
					languageSelect.setDisable(true);
					namespaceSelect.setDisable(true);
					removeButton.setDisable(true);
					
					titleEdit.setText("");
					languageSelect.getSelectionModel().select(-1);
					
				}
			 }
		});
	 }
	
	@Override
	public void exposeConfirm(final Button button) {
		contentTable.getItems().addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable observable) {
				button.setDisable(contentTable.getItems().isEmpty());
			}
		});
		button.setDisable(contentTable.getItems().isEmpty());
	}
	
	
	// IMPLEMENT (CONTENT)
	public List<ContentRequest> collectContentRequests()
	 {
		ArrayList<ContentRequest> requests = new ArrayList<>();
		
		ObservableList<ContentRequestFX> items = contentTable.getItems();
		for (ContentRequestFX item : items) {
			try {
				ContentRequest request = item.toContentRequest();
				if (request.isCategory()) request = new ContentSubRequest(request, expandCategoriesCheck.isSelected(), addArticlesCheck.isSelected(), addCategoriesCheck.isSelected());
				requests.add(request);
			} catch (Exception e) {
				LOGGER.error("Failed to convert request", e);
			}
		}
		
		return requests;
	 }
	
	
	// INHERIT (CONFIRM DECLINE DIALOG)
	@Override
	public void focus() {
		super.focus();
		addButton.requestFocus();
	}
	
	// EVENTS
	@FXML
	void onAddClick()
	 {
		Integer oldNamespaceIndex = namespaceSelect.getSelectionModel().getSelectedIndex();
		if (oldNamespaceIndex == -1) oldNamespaceIndex = 0;
		
		ContentRequestFX item = new ContentRequestFX();
		if (contentTable.getItems().add(item)) {
			contentTable.getSelectionModel().select(item);
		}
		
		if (namespaceSelect.getSelectionModel().getSelectedIndex() == -1) {
			namespaceSelect.getSelectionModel().select(oldNamespaceIndex);
		}
		
		titleEdit.requestFocus();
	 }
	
	@FXML
	void onRemoveClick()
	 {
		ObservableList<ContentRequestFX> items = contentTable.getSelectionModel().getSelectedItems();
		contentTable.getItems().removeAll(items);
	 }
	
	@FXML
	void onPasteClick(ActionEvent event) {
		Clipboard clipboard = Clipboard.getSystemClipboard();
        if (clipboard.hasString()) {
        	String[] lines = clipboard.getString().split("\n");
        	for (String line : lines) {
        		line = line.replace('\t', ' ');
        		line = line.trim();
        		if (line.isEmpty()) continue;
        		if (!titleEdit.getText().isEmpty() || contentTable.getSelectionModel().getSelectedIndex() == -1) onAddClick(); // simulate add click
        		titleEdit.setText(line);
			}
        }
	}
	
	
}
