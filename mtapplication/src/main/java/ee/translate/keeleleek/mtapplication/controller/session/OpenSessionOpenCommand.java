package ee.translate.keeleleek.mtapplication.controller.session;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.Notifications;

public class OpenSessionOpenCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		sendNotification(Notifications.WINDOW_OPEN_SESSION_OPEN);
	 }
	
}
