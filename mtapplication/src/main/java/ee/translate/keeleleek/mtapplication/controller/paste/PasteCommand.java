package ee.translate.keeleleek.mtapplication.controller.paste;

import java.util.regex.PatternSyntaxException;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.Status;
import ee.translate.keeleleek.mtapplication.model.content.Reference;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;
import ee.translate.keeleleek.mtapplication.view.windows.TranslateWindowMediator;

public class PasteCommand extends SimpleCommand {

	private static Logger LOGGER = LoggerFactory.getLogger(PasteCommand.class);
	
	
	@Override
	public void execute(INotification notification)
	 {
		TranslateWindowMediator window = (TranslateWindowMediator) getFacade().retrieveMediator(TranslateWindowMediator.NAME);
		Reference ref = window.getCurrentReference();
		if (ref == null) return;
		
		Integer index = (Integer) notification.getBody();
		
		// check article
		MinorityArticle article = MinorityTranslateModel.content().getArticle(ref);
		if (article == null) {
			LOGGER.error("Failed to paste, because the article with reference " + ref + " is missing!");
			return;
		}
		
		// check status
		if (article.getStatus() != Status.STANDBY) {
			sendNotification(Notifications.ERROR_MESSAGE, Messages.getString("messages.paste.header.pulling.failed"), Messages.getString("messages.paste.description.target.not.editable"));
			return;
		}
		
		// paste
		String pasteText = MinorityTranslateModel.preferences().findSnippet(index, ref.getLangCode());
		
		try {
			
			Reference dstRef = article.getRef();
			
			String langCode = dstRef.getLangCode();
			String title = article.getTitle();
			String text = article.getText();
			
			pasteText = MinorityTranslateModel.processer().pasteProcess(langCode, title, text, pasteText);
			
			sendNotification(Notifications.PROCESSING_PASTE, pasteText);
			
		} catch (PatternSyntaxException e) {
			sendNotification(Notifications.ERROR_MESSAGE, Messages.getString("messages.paste.header.pulling.failed"), Messages.getString("messages.paste.description.regex.error").replaceFirst("#description", e.getDescription()).replaceFirst("#pattern", e.getPattern()));
		}
	 }
	
}
