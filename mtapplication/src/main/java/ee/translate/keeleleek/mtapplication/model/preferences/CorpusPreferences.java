package ee.translate.keeleleek.mtapplication.model.preferences;

import java.util.HashMap;

import ee.translate.keeleleek.mtapplication.model.preferences.PreferencesProxy.Proficiency;

/**
 * Immutable corpus related preferences.
 */
public class CorpusPreferences {

	private HashMap<String, Proficiency> proficiencies;
	private Boolean collect;
	private Boolean exact;
	
	
	// INIT
	public CorpusPreferences()
	 {
		this.proficiencies = new HashMap<>();
		this.collect = true;
		this.exact = false;
	 }
	
	public CorpusPreferences(HashMap<String, Proficiency> proficiencies, Boolean collect, Boolean exact)
	 {
		this.proficiencies = proficiencies;
		this.collect = collect;
		this.exact = exact;
	 }

	public CorpusPreferences(CorpusPreferences corpus)
	 {
		this.proficiencies = new HashMap<>(corpus.proficiencies);
		this.collect = corpus.collect;
		this.exact = corpus.exact;
	 }

	public static CorpusPreferences create()
	 {
		CorpusPreferences preferences = new CorpusPreferences();
		
		preferences.collect = true;
		preferences.exact = false;
		
		return preferences;
	 }
	

	// PREFERENCES
	public HashMap<String, Proficiency> getProficiencies() {
		return proficiencies;
	}
	
	public Proficiency getProficiency(String langCode) {
		Proficiency proficiency = proficiencies.get(langCode);
		if (proficiency == null) proficiency = Proficiency.UNSPECIFIED;
		return proficiency;
	}
	
	public Boolean isCollect() {
		return collect;
	}

	public CorpusPreferences withProficiency(String langCode, Proficiency proficiency) {
		CorpusPreferences preferences = new CorpusPreferences(this);
		preferences.proficiencies.put(langCode, proficiency);
		return preferences;
	}

	public CorpusPreferences withCollect(Boolean collect) {
		CorpusPreferences preferences = new CorpusPreferences(this);
		preferences.collect = collect;
		return preferences;
	}

	public Boolean isExact() {
		return exact;
	}
	
	
	// UTILITY
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((collect == null) ? 0 : collect.hashCode());
		result = prime * result + ((exact == null) ? 0 : exact.hashCode());
		result = prime * result + ((proficiencies == null) ? 0 : proficiencies.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		CorpusPreferences other = (CorpusPreferences) obj;
		if (collect == null) {
			if (other.collect != null) return false;
		} else if (!collect.equals(other.collect)) return false;
		if (exact == null) {
			if (other.exact != null) return false;
		} else if (!exact.equals(other.exact)) return false;
		if (proficiencies == null) {
			if (other.proficiencies != null) return false;
		} else if (!proficiencies.equals(other.proficiencies)) return false;
		return true;
	}

	
}
