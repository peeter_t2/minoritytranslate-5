package ee.translate.keeleleek.mtapplication.view.javafx.editors;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;

import ee.translate.keeleleek.mtapplication.common.requests.FindReplaceRequest;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.autocomplete.AutocompleteChoice;
import ee.translate.keeleleek.mtapplication.model.autocomplete.AutocompleteChoices;
import ee.translate.keeleleek.mtapplication.model.autocomplete.AutocompleteRequest;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.Status;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.TitleStatus;
import ee.translate.keeleleek.mtapplication.model.media.IconsProxyFX;
import ee.translate.keeleleek.mtapplication.model.preferences.Symbol;
import ee.translate.keeleleek.mtapplication.view.elements.EditorMediator;
import ee.translate.keeleleek.mtapplication.view.javafx.elements.AceEditWrapper;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;
import ee.translate.keeleleek.mtpluginframework.spellcheck.Misspell;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.concurrent.Worker.State;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.web.WebView;


public class AlignEditorMediatorFX extends EditorMediator {

	public static final String PREVIEW_STYLESHEET_PATH = EditorMediator.class.getResource("/css/wiki.css").toExternalForm();
	
	
	// edits and views
	@FXML
	private Parent editBox;
	@FXML
	private TextField titleEdit;
	@FXML
	private WebView previewView;
	@FXML
	protected WebView textEdit1;
	@FXML
	protected WebView textEdit2;
	@FXML
	protected WebView textEdit3;
	@FXML
	protected WebView textEdit4;
	
	protected WebView[] textEditors;
	
	// ace editor
	private AceEditWrapper[] aceEditors;
	
	@FXML
	private Pane translateBox;
	
	@FXML
	private Label statusLabel;
	
	@FXML
	private Button resetButton;
	@FXML
	private MenuButton spellerSelect;
	@FXML
	private ToggleButton previewButton;
	
	@FXML
	private Parent editBar;
	@FXML
	private Parent filterBar;
	
	// symbols
	@FXML
	private HBox row0Box;
	@FXML
	private HBox row1Box;
	@FXML
	private HBox row2Box;
	@FXML
	private CheckBox exactCheckbox;

	// text
	private int offsetIndex = 0;
	private List<String> editorsText = new ArrayList<>();
	
	// options
	@FXML
	private CheckBox filterTemplatesCheckbox;
	@FXML
	private CheckBox filterFilesCheckbox;
	@FXML
	private CheckBox filterIntroductionCheckbox;
	@FXML
	private CheckBox filterReferencesCheckbox;
	
	@FXML
	private ProgressIndicator titleCheckIndicator;
	
	private String previewScrollYOffset = null;
	
	boolean feedback = false;
	boolean titleEditable = false;
	boolean textEditable = false;

	
	/* ******************
	 *                  *
	 *  Initialization  *
	 *                  *
	 ****************** */
	public AlignEditorMediatorFX(EditorMode mode)
	 {
		super(mode);
	 }
	
	@FXML
	public void initialize()
	 {
		initialiseAceEdit();
		
		initialiseEditors();
		
		switch (getMode()) {
		case NORMAL:
			editBar.setVisible(true);
			filterBar.setVisible(false);
			spellerSelect.setVisible(true);
			break;

		case FILTERED:
			editBar.setVisible(false);
			filterBar.setVisible(true);
			spellerSelect.setVisible(false);
			break;

		default:
			break;
		}
		
		List<String> spellerPluginNames = MinorityTranslateModel.speller().getPluginNames(MinorityTranslateModel.preferences().getGUILangCode());
		
		spellerSelect.getItems().clear();

		boolean first = true;
		ToggleGroup toggleGroup = new ToggleGroup();
		for (String pluginName : spellerPluginNames) {
			RadioMenuItem item = new RadioMenuItem(pluginName);
			if (first) {
				item.setSelected(true);
				first = false;
			}
			item.setToggleGroup(toggleGroup);
			
			item.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					onSpellCheckRequest();
				}
			});
			
			spellerSelect.getItems().add(item);
		}
	 }
	
	private void initialiseAceEdit()
	 {
		textEditors = new WebView[]{textEdit1, textEdit2, textEdit3, textEdit4};
		aceEditors = new AceEditWrapper[textEditors.length];
		
		for (int i = 0; i < textEditors.length; i++) {
			
			WebView textEdit = textEditors[i];
			aceEditors[i] = new AceEditWrapper(textEdit) {
				@Override
				public void onUserEditText() {
					onTextEdited();
				}
				
				@Override
				public void onUserRequestAutocomplete(String preceedingText, String selectedText) {
					onAutocomplete(new AutocompleteRequest(getReference(), preceedingText, selectedText));
				}
			};
			
		}
	 }
	
	private void initialiseEditors()
	 {
		titleEdit.textProperty().addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable observable)
			 {
				if (feedback) return;
				onTitleEdited();
			 }
		});

		previewView.getEngine().getLoadWorker().stateProperty().addListener(
		new ChangeListener<State>() {
			@Override
			public void changed(ObservableValue<? extends State> ov, State oldState, State newState)
			 {
				if (newState != State.SUCCEEDED) return;
				
				if (previewScrollYOffset != null) previewView.getEngine().executeScript("window.scrollTo(0, " + previewScrollYOffset + ")");
				
				previewView.getEngine().setUserStyleSheetLocation(PREVIEW_STYLESHEET_PATH);
			 }
			
		});
	 }
	
	
	
	/* ******************
	 *                  *
	 *      Edits       *
	 *                  *
	 ****************** */
	private AceEditWrapper findFocusedEdit() {
		for (int i = 0; i < aceEditors.length; i++) {
			if (textEditors[i].isFocused()) return aceEditors[i];
		}
		return null;
	}
	
	@Override
	public void setTabView(EditorView view)
	 {
		switch (view) {
		case EDIT:
			editBox.setVisible(true);
			previewView.setVisible(false);
			editBox.requestFocus();
			previewButton.setSelected(false);
			break;
			
		case PREVIEW:
			editBox.setVisible(false);
			previewView.setVisible(true);
			previewView.requestFocus();
			previewButton.setSelected(true);
			break;

		default:
			break;
		}
	 }
	
	public void setTextEditEditable(boolean editable)
	 {
		for (int i = 0; i < aceEditors.length; i++) {
			aceEditors[i].setTextEditEditable(editable);
		}
	 }
	
	protected void addStylesheet(Document doc, String externalForm) {
		for (int i = 0; i < aceEditors.length; i++) {
			aceEditors[i].addStylesheet(doc, externalForm);
		}
	}

	@Override
	public boolean isEditFocused()
	 {
		return textEdit1.isFocused() || textEdit2.isFocused() || textEdit3.isFocused() || textEdit4.isFocused() || previewView.isFocused();
	 }
	

	
	/* ******************
	 *                  *
	 *      Index       *
	 *                  *
	 ****************** */
	public void changeEditorIndex(int editorIndex)
	 {
		if (this.offsetIndex == editorIndex) return;
		
		this.offsetIndex = editorIndex;
		
		setEditorText();
	 }
	
	public int findEditorMaxIndex()
	 {
		return editorsText.size() - 1;
	 }
	
	
	/* ******************
	 *                  *
	 *      Title       *
	 *                  *
	 ****************** */
	public String getTitle() {
		return titleEdit.getText();
	}
	
	public void setTitle(String title) {
		feedback = true;
		titleEdit.setText(title);
		feedback = false;
	}
	
	
	
	/* ******************
	 *                  *
	 *       Text       *
	 *                  *
	 ****************** */
	public void flushText()
	 {
		// flush editors
		for (int i = aceEditors.length - 1; i >= 0; i--) {
			
			String text = aceEditors[i].getText();
			
			int offsetIndex = this.offsetIndex + i;
			while (editorsText.size() <= offsetIndex) editorsText.add(""); // adjust size
			
			editorsText.set(offsetIndex, text);
			
		}
		
		// do aligns
		boolean realign = false;
		for (int i = aceEditors.length - 1; i >= 0; i--) {
			
			int offsetIndex = this.offsetIndex + i;

			String text = editorsText.get(offsetIndex);
			
			// collapse aligns
			while (!text.endsWith("\n")) {
				
				if (offsetIndex + 1 >= editorsText.size()) break;
				
				text+= editorsText.remove(offsetIndex + 1);
				editorsText.set(offsetIndex, text);
				
				realign = true;
				
			}
			
			// expand aligns
			List<String> aligns = MinorityTranslateModel.preferences().getTextAligner().alignTextPartial(text);
			
			if (aligns.size() != 1) {
				
				editorsText.set(offsetIndex, aligns.get(0));
				
				for (int j = aligns.size() - 1; j > 0 ; j--) {
					
					String align = aligns.get(j);
					editorsText.add(offsetIndex + 1, align);
					
				}
				
				realign = true;
			}
			
		}

		// remove trailing empty
		while (editorsText.size() > 0) {
			if (editorsText.get(editorsText.size() - 1).isEmpty()) {
				editorsText.remove(editorsText.size() - 1);
			}
			else break;
		}

		// update editor
		if (realign) setEditorText();
	 }
	
	public String getText()
	 {
		ArrayList<String> trimmedText = new ArrayList<>(editorsText); // trimming
		while (trimmedText.size() > 0) {
			String text = trimmedText.get(trimmedText.size() - 1);
			if (text.isEmpty() || text.equals("\n")) {
				trimmedText.remove(trimmedText.size() - 1);
			} else {
				break;
			}
		}
		
		return MinorityTranslateModel.preferences().getTextAligner().unalignText(trimmedText);
	 }

	public void setText(String text)
	 {
		editorsText = MinorityTranslateModel.preferences().getTextAligner().alignText(text);
		setEditorText();
	 }

	private void setEditorText()
	 {
		for (int i = 0; i < aceEditors.length; i++) {
			
			int j = offsetIndex + i;
			if (j >= 0 && j < editorsText.size()) {
				aceEditors[i].setText(editorsText.get(j));
			} else {
				aceEditors[i].setText("\n"); // will be trimmed
			}
			
		}
	 }

	private void replaceEditorText()
	 {
		for (int i = 0; i < aceEditors.length; i++) {
			
			int j = offsetIndex + i;
			if (j >= 0 && j < editorsText.size()) {
				aceEditors[i].replaceText(editorsText.get(j));
			} else {
				aceEditors[i].replaceText("\n");
			}
			
		}
	 }
	
	public void replaceText(String text)
	 {
		editorsText = MinorityTranslateModel.preferences().getTextAligner().alignText(text);
		replaceEditorText();
	 }

	public void pasteText(String text)
	 {
		AceEditWrapper edit = findFocusedEdit();
		if (edit != null) edit.pasteText(text);
	 }
	
	
	
	/* ******************
	 *                  *
	 *     Preview      *
	 *                  *
	 ****************** */
	@Override
	public void setPreview(String html) {
		previewScrollYOffset = previewView.getEngine().executeScript("window.pageYOffset").toString();
		previewView.getEngine().loadContent(html);
	}
	
	
	
	/* ******************
	 *                  *
	 *      Status      *
	 *                  *
	 ****************** */
	@Override
	public void setStatus(Status status)
	 {
		if (status != null) {
			
			String text = Messages.getString("statusbar." + status.toString().toLowerCase().replace('_', '.'));
			statusLabel.setTooltip(new Tooltip(text));
			
			IconsProxyFX icons = (IconsProxyFX) MinorityTranslateModel.icons();
			Image icon = icons.getStatusIcon(status);
			ImageView image = icon != null ? new ImageView(icon) : null;
			statusLabel.setGraphic(image);
			
			statusLabel.setVisible(true);
			
		} else {
			statusLabel.setText("");
			statusLabel.setVisible(false);
		}
		
		boolean disable = status == null;
		
		titleEditable = isArticleNew() && (status == Status.STANDBY) && getMode() == EditorMode.NORMAL;
		textEditable = (status == Status.STANDBY) && getMode() == EditorMode.NORMAL;
		
		titleEdit.setEditable(titleEditable);
		setTextEditEditable(textEditable);
		
		exactCheckbox.setDisable(!textEditable || disable);
	 }

	@Override
	public void setTitleStatus(TitleStatus status)
	 {
		if (status == null) status = TitleStatus.UNKNOWN;
		
		
		switch (status) {
		case CONFLICT:
			titleEdit.getStyleClass().add("problem");
			break;

		default:
			titleEdit.getStyleClass().removeAll("problem");
			break;
		}
		
		if (status == TitleStatus.CHECKING || status == TitleStatus.CHECKING_REQUIRED) {
			
			titleCheckIndicator.setVisible(true);
			titleCheckIndicator.setManaged(true);
			
		} else {
			
			titleCheckIndicator.setVisible(false);
			titleCheckIndicator.setManaged(false);
			
		}
	 }
	
	
	
	/* ******************
	 *                  *
	 *   Autocomplete   *
	 *                  *
	 ****************** */
	public void showAutocomplete(AutocompleteChoices autocomplete)
	 {
		AceEditWrapper edit = findFocusedEdit();
		if (edit != null) edit.showAutocomplete(autocomplete);
	 }
   
	public void insertAutocomplete(AutocompleteChoice choice)
	 {
		AceEditWrapper edit = findFocusedEdit();
		if (edit != null) edit.insertAutocomplete(choice);
	 }
   
	

	/* ******************
	 *                  *
	 *   Spell check    *
	 *                  *
	 ****************** */
	@Override
	public String getSpellerPluginName() {
		ObservableList<MenuItem> menu = spellerSelect.getItems();
		for (MenuItem menuItem : menu) {
			if (menuItem instanceof RadioMenuItem) {
				if (((RadioMenuItem) menuItem).isSelected()) return menuItem.getText();
			}
		}
		return null;
	}
	
	public void markMisspells(List<Misspell> misspells)
	 {
		//TODO aceEdit1.markMisspells(misspells);
	 }
	
	

	/* ******************
	 *                  *
	 *   Find/Replace   *
	 *                  *
	 ****************** */	
	public void findReplace(FindReplaceRequest request)
	 {
		AceEditWrapper edit = findFocusedEdit();
		if (edit != null) edit.findReplace(request);
	 }
	
	
	
	/* ******************
	 *                  *
	 *     Symbols      *
	 *                  *
	 ****************** */	
	@Override
	protected void setSymbols(List<Symbol> symbols)
	 {
		row0Box.getChildren().clear();
		row1Box.getChildren().clear();
		row2Box.getChildren().clear();
		
		for (Symbol symbol : symbols) {
			Hyperlink link = new Hyperlink(symbol.getSymbol().toString());
			link.setFocusTraversable(false);
			link.setVisited(true);
			link.setMinHeight(10);
			link.setPadding(new Insets(0, 2.5, 0, 2.5));
			String name = symbol.getName();
			if (!symbol.getTrigger().isEmpty()) name+= " (" + symbol.getTrigger() + " & Ctrl+Space)";
			link.setTooltip(new Tooltip(name));
			link.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					pasteText(((Hyperlink) event.getSource()).getText());
				}
			});
			
			switch (symbol.getRow()) {
			case 0:
				row0Box.getChildren().add(link);
				break;

			case 1:
				row1Box.getChildren().add(link);
				break;

			case 2:
				row2Box.getChildren().add(link);
				break;

			default:
				break;
			}
			
		}
	 }

	
	
	/* ******************
	 *                  *
	 *     Options      *
	 *                  *
	 ****************** */	
	@Override
	public void setTranslationExact(boolean exact) {
		exactCheckbox.setSelected(exact);
	}
	
	@Override
	protected void setFilterTemplates(boolean filter) {
		filterTemplatesCheckbox.setSelected(filter);
	}
	
	@Override
	protected void setFilterFiles(boolean filter) {
		filterFilesCheckbox.setSelected(filter);
	}
	
	@Override
	protected void setFilterIntroduction(boolean filter) {
		filterIntroductionCheckbox.setSelected(filter);
	}
	
	@Override
	protected void setFilterReferences(boolean filter) {
		filterReferencesCheckbox.setSelected(filter);
	}
	
	
	
	/* ******************
	 *                  *
	 *      Events      *
	 *                  *
	 ****************** */
	@Override
	public void onBeforeTextUpdate() {
		flushText();
	}
	
	@Override
	public void onReferenceChanged() {
		previewScrollYOffset = null;
	}
	
	@FXML
	protected void onResetClick() {
		onReset();
	}
	
	@FXML
	protected void onPreviewToggle() {
		if (previewButton.isSelected()) super.onChangeView(EditorView.PREVIEW);
		else super.onChangeView(EditorView.EDIT);
	}

	@FXML
	protected void onExactCheck() {
		super.onChangeExact(exactCheckbox.isSelected());
	}

	@FXML
	protected void onFilterTemplatesCheck() {
		super.onChangeTemplatesFilter(filterTemplatesCheckbox.isSelected());
	}

	@FXML
	protected void onFilterFilesCheck() {
		super.onChangeFilesFilter(filterFilesCheckbox.isSelected());
	}

	@FXML
	protected void onFilterIntroductionCheck() {
		super.onChangeIntroductionFilter(filterIntroductionCheckbox.isSelected());
	}

	@FXML
	protected void onFilterReferencesCheck() {
		super.onChangeReferencesFilter(filterReferencesCheckbox.isSelected());
	}
	
	
}
