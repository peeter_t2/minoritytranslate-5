package ee.translate.keeleleek.mtapplication.model.content;

import java.util.ArrayList;
import java.util.Queue;

import org.puremvc.java.multicore.patterns.proxy.Proxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.MinorityTranslate;
import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.Status;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.WikiType;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;
import net.sourceforge.jwbf.core.contentRep.Article;
import net.sourceforge.jwbf.mediawiki.bots.MediaWikiBot;

public class ContentUploaderProxy extends Proxy implements Runnable {

	public final static String NAME = "{D6BB6E96-9FA2-45B2-ADED-66BE2B821AEC}";
	
	private static Logger LOGGER = LoggerFactory.getLogger(ContentUploaderProxy.class);
	
	public final static long TIMEOUT = 250*0 + 1000;
	public final static int MAX_UPLOADS = 10;
	
	
	private ArrayList<FullRequest> uploading = new ArrayList<>();
	
	private boolean askedLogin = false;
	
	private boolean busy = false;
	private boolean run = false;

	
	// INIT
	public ContentUploaderProxy() {
		super(NAME, "");
	}

	@Override
	public void onRegister() {
		run = true;
		
		startThread();
	}
	
	@Override
	public void onRemove() {
		run = false;
	}

	private void startThread()
	 {
		Thread thread = new Thread(this);
		thread.setDaemon(true);
		thread.start();
	 }
	
	
	// WORK
	@Override
	public void run()
	 {
		try {
			while (run) {
				work();
				Thread.sleep(TIMEOUT);
			}
		}
		catch (InterruptedException e) {
			LOGGER.error("Thread sleep failure!", e);
		}
		catch (IllegalStateException e) {
			LOGGER.warn("Failed to upload content: " + e.getMessage() + "!");
			startThread();
		}
	 }
	
	private void work()
	 {
		lookBusy();
		
		if (uploading.size() >= MAX_UPLOADS) return;
		
		Queue<MinorityArticle> articles = ContentUtil.filterArticles(MinorityTranslateModel.content(), Status.UPLOAD_REQUESTED);
		while (!articles.isEmpty() && uploading.size() < MAX_UPLOADS) {
			
			MinorityArticle article = articles.remove();
			upload(article.createFullRef());
        
		}
		
		lookBusy();
	 }

	private void lookBusy()
	 {
		boolean busy = uploading.size() > 0;
		
		if (this.busy == busy) return;
		
		this.busy = busy;
		
		MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.ENGINE_UPLOADER_BUSY, busy);
	 }
	
	
	// DOWNLOADING
	private void upload(final FullRequest fullRef)
	 {
		if (!MinorityTranslateModel.session().isUploadEnabled()) return;
		
		if (!checkLogin(fullRef.getLangCode())) {
			askLogin();
			return;
		}
		
		Thread thread = new Thread(new Runnable() {
			
			@Override
			public void run()
			 {
				if (uploading.contains(fullRef)) return;
				
				// preparing
				Reference ref = fullRef.createReference();
				String langCode = fullRef.getLangCode();
				String wikiTitle = fullRef.getWikiTitle();
				
				uploading.add(fullRef);
				MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.ENGINE_UPLOAD_STARTED, ref);
				MinorityTranslateModel.content().changeStatus(ref, Status.UPLOAD_PREPARING);
	
				// retrieving article
				MinorityArticle article = MinorityTranslateModel.content().getArticle(ref);
				if (article == null) {
					LOGGER.error("Uploading failed, because the article is missing!");
					uploading.remove(fullRef);
					return;
				}

				// post-processing
				MinorityTranslateModel.processer().handleProcess(article);
			
				// checking conflicts
				LOGGER.info("Checking upload conflicts for article with reference " + ref);
				
				String prevrev = article.getPreviousRevision();
				MediaWikiBot bot = MinorityTranslateModel.bots().fetchBot(langCode);
				Article wikiArticle = bot.getArticle(wikiTitle);
				String currev = wikiArticle.getRevisionId();
				
				if (!prevrev.equals(currev)) {
					LOGGER.error("Upload conflict for article " + article + "!");
					MinorityTranslateModel.content().changeStatus(ref, Status.UPLOAD_CONFLICT);
					uploading.remove(fullRef);
					return;
				}
				
				// summary
				String summary = "Edited with MinorityTranslate";
				String srcLangCode = article.getSrcLangCode();
				if (srcLangCode != null) {
					
					Reference refSrc = ref.withLangCode(srcLangCode);
					MinorityArticle srcArticle = MinorityTranslateModel.content().getArticle(refSrc);
					if (srcArticle != null) summary+= " using article [" + srcLangCode + ":" + srcArticle.getTitle() + "]";

				}
				
				// uploading
				LOGGER.info("Uploading article " + article);
				MinorityTranslateModel.content().changeStatus(ref, Status.UPLOAD_IN_PROGRESS);
				
				if (!MinorityTranslate.NEUTERED) {
					wikiArticle.setTitle(wikiTitle);
					wikiArticle.setText(article.getText());
					wikiArticle.setEditSummary(summary);
					wikiArticle.save();
				} else {
					LOGGER.info("Upload ignored, becuase of neutered mode!");
				}

				// linking
				WikiType wikiType = MinorityTranslateModel.wikis().getWikiType(langCode);
				switch (wikiType) {
				case REGULAR:
					LOGGER.info("Linking titles for article with reference " + ref);
					MinorityTranslateModel.content().changeStatus(ref, Status.LINKING_ARTICLES);
					
					if (article.isLinkingRequired()) {
						if (!MinorityTranslate.NEUTERED) {
							String origLangCode = article.getOriginalWikiReference().getLangCode();
							String origTitle = article.getOriginalWikiReference().getWikiTitle();
							if (!langCode.equals(origLangCode)) performArticleLink(origLangCode, origTitle, langCode, wikiTitle);
						} else {
							LOGGER.info("Linking ignored, becuase of neutered mode!");
						}
					}
					
					break;
					
				case INCUBATOR:
					LOGGER.info("Linking ignored for incubator " + ref);
					break;
					
				default:
					LOGGER.error("Linking ignored for unknown wiki type");
					break;
				}
				
				// confirming
				LOGGER.info("Conforming upload for article with reference " + ref);
				MinorityTranslateModel.content().changeStatus(ref, Status.UPLOAD_CONFIRMING);
				
				wikiArticle = bot.getArticle(wikiTitle);
				String nextrev = wikiArticle.getRevisionId();
				
				if (nextrev.equals(currev)) {
					LOGGER.error("Upload failure for article " + article + "!");
					MinorityTranslateModel.content().changeStatus(ref, Status.UPLOAD_FAILED);
					uploading.remove(fullRef);
					return;
				}
				
				MinorityTranslateModel.content().setOriginal(ref);

				// keeleleek
				if (MinorityTranslateModel.preferences().corpus().isCollect())
				 {
					LOGGER.info("Sending usage info for article with reference " + ref);
					MinorityTranslateModel.content().changeStatus(ref, Status.SENDING_USAGE_INFO);
					
					MinorityTranslateModel.usage().updateNewRev(ref, nextrev);
					MinorityTranslateModel.usage().updateUsername(ref, bot.getUserinfo().getUsername());
					MinorityTranslateModel.usage().updateTag(ref, bot.getUserinfo().getUsername());
					
					if (!MinorityTranslate.NEUTERED) {
						MinorityTranslateModel.usage().send(ref);
					} else {
						LOGGER.info("Sending usage info ignored, becuase of neutered mode!");
					}
				 }
				
				// finishing
				uploading.remove(fullRef);
				MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.ENGINE_UPLOAD_ENDED, ref);
				MinorityTranslateModel.content().changeStatus(ref, Status.STANDBY);
				
				LOGGER.info("Uploading complete for article with reference " + ref);
			 }
		});
		
		LOGGER.info("Requesting upload for " + fullRef);
		thread.start();
	 }
	
	
	// LOGIN
	private boolean checkLogin(String langCode) {
		return MinorityTranslateModel.bots().getWikidataBot().isLoggedIn() && MinorityTranslateModel.bots().fetchBot(langCode).isLoggedIn();
	}
	
	private void askLogin()
	 {
		if (askedLogin) return;
		
		MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.WINDOW_LOGIN_OPEN);
		
		askedLogin = true;
	 }
	
	
	// LINKING
	private boolean performArticleLink(String fromLangCode, String fromTitle, String toLangCode, String toTitle)
	 {
		try {
			MinorityTranslateModel.bots().linkInterlang(fromLangCode, fromTitle, toLangCode, toTitle);
			return true;
		} catch (Exception e) {
			MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.ERROR_MESSAGE, Messages.getString("messages.linking.failed.header"), Messages.getString("messages.linking.failed.content.message").replace("#message", e.getLocalizedMessage()));
			return false;
		}
	 }
	
	
}
