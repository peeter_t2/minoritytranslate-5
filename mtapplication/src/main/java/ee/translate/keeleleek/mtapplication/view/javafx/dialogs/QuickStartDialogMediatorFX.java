package ee.translate.keeleleek.mtapplication.view.javafx.dialogs;


import java.net.URL;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.common.FileUtil;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.preferences.CorpusPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.LanguagesPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.PreferencesProxy.Display;
import ee.translate.keeleleek.mtapplication.model.preferences.PreferencesProxy.Proficiency;
import ee.translate.keeleleek.mtapplication.model.preferences.DisplayPreferences;
import ee.translate.keeleleek.mtapplication.view.dialogs.QuickStartDialogMediator;
import ee.translate.keeleleek.mtapplication.view.elements.EditorMediator;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Side;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;
import javafx.scene.web.WebView;


public class QuickStartDialogMediatorFX extends QuickStartDialogMediator {

	private static String QUICK_START_STYLESHEET_PATH = EditorMediator.class.getResource("/guide/guide.css").toExternalForm();
	
	@FXML
	private WebView htmlView;

	@FXML
	private StackPane detailsBox;

	@FXML
	private Node interfaceLanguagePage;
	@FXML
	private ComboBox<String> languagesCombobox;

	@FXML
	private Node languagesPage;
	@FXML
	private TextField srcLanguageEdit;
	@FXML
	private TextField dstLanguageEdit;
	
	@FXML
	private Node textCorpusPage;
	@FXML
	private Label srcLangLabel;
	@FXML
	private Label dstLangLabel;
	@FXML
	private ComboBox<String> destinationProficiencyComboBox;
	@FXML
	private ComboBox<String> sourceProficiencyComboBox;
	@FXML
	private CheckBox collectCorpusCheckbox;
	
	private ContextMenu srcLanguageMenu = null;
	private ContextMenu dstLanguageMenu = null;
	
	private boolean block = false;
	
	
	// INIT
	@FXML
	private void initialize()
	 {
		// style
		htmlView.getEngine().setUserStyleSheetLocation(QUICK_START_STYLESHEET_PATH);
		
		// GUI languages
		String[] guiLangCodes = Messages.getLangCodes();
		String[] guiLangNames = MinorityTranslateModel.wikis().getLangNames(guiLangCodes);
		String guiLangCode = MinorityTranslateModel.preferences().getGUILangCode();
		String guiLangName = MinorityTranslateModel.wikis().getLangName(guiLangCode);
		languagesCombobox.setItems(FXCollections.observableArrayList(guiLangNames));
		languagesCombobox.getSelectionModel().select(guiLangName);
		
		// Source language
		srcLanguageEdit.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> obs, String oldVal, String newVal)
			 {
				String langCode = getLangCode(newVal);
				if (langCode == null && !newVal.isEmpty() || newVal.equalsIgnoreCase(dstLanguageEdit.getText())) srcLanguageEdit.setStyle("-fx-focus-color: red ; -fx-faint-focus-color: transparent ; -fx-text-box-border: red ;");
				else srcLanguageEdit.setStyle(null);

				if (block) return;
				
				final String[] langNames = MinorityTranslateModel.wikis().findSuggestions(newVal);
				MenuItem[] items = new MenuItem[langNames.length];
				for (int i = 0; i < langNames.length; i++) {
					
					final String langName = langNames[i];
					items[i] = new MenuItem(langNames[i]);
					items[i].setOnAction(new EventHandler<ActionEvent>()
					 {
						@Override
						public void handle(ActionEvent event) {
							block = true;
							srcLanguageEdit.setText(langName);
							srcLanguageEdit.positionCaret(langName.length());
							block = false;
						}
					 });
				}
				
				if (srcLanguageMenu != null) srcLanguageMenu.hide();
				if (items.length > 0) {
					srcLanguageMenu = new ContextMenu(items);
					srcLanguageMenu.show(srcLanguageEdit, Side.BOTTOM, 0, 0);
				}
			 }
		});
		
		// Destination language
		dstLanguageEdit.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> obs, String oldVal, String newVal)
			 {
				String langCode = getLangCode(newVal);
				if (langCode == null && !newVal.isEmpty()) dstLanguageEdit.setStyle("-fx-focus-color: red ; -fx-faint-focus-color: transparent ; -fx-text-box-border: red ;");
				else dstLanguageEdit.setStyle(null);
				
				if (block) return;
				
				final String[] langNames = MinorityTranslateModel.wikis().findSuggestions(newVal);
				MenuItem[] items = new MenuItem[langNames.length];
				for (int i = 0; i < langNames.length; i++) {
					
					final String langName = langNames[i];
					items[i] = new MenuItem(langNames[i]);
					items[i].setOnAction(new EventHandler<ActionEvent>()
					 {
						@Override
						public void handle(ActionEvent event) {
							block = true;
							dstLanguageEdit.setText(langName);
							dstLanguageEdit.positionCaret(langName.length());
							block = false;
						}
					 });
				}
				
				if (dstLanguageMenu != null) dstLanguageMenu.hide();
				if (items.length > 0) {
					dstLanguageMenu = new ContextMenu(items);
					dstLanguageMenu.show(dstLanguageEdit, Side.BOTTOM, 0, 0);
				}
			 }
		});
		
		// Proficiencies
		ObservableList<String> proficiencies = FXCollections.observableArrayList(
				Messages.getString("preferences.program.corpus.proficiency.unspecified"),
				Messages.getString("preferences.program.corpus.proficiency.level1"),
				Messages.getString("preferences.program.corpus.proficiency.level2"),
				Messages.getString("preferences.program.corpus.proficiency.level3"),
				Messages.getString("preferences.program.corpus.proficiency.level4"),
				Messages.getString("preferences.program.corpus.proficiency.level5")
		);

		// Source proficiency
		sourceProficiencyComboBox.setItems(proficiencies);
		
		// Destination proficiency
		destinationProficiencyComboBox.setItems(proficiencies);
		
		// hide
		collectCorpusCheckbox.setVisible(false);
		collectCorpusCheckbox.setManaged(false);
	 }
	
	
	// INHERIT (PAGES)
	@Override
	public void show(String pageName)
	 {
		String pagePath = "empty";
		String srcLangCode = null;
		String dstLangCode = null;
		String srcLangName = null;
		String dstLangName = null;

		ObservableList<Node> childeren = detailsBox.getChildren();
		for (Node node : childeren) {
			node.setVisible(false);
			node.setManaged(false);
		}
		
		switch (pageName) {
		case INTERFACE_LANGUAGE_PAGE:
			pagePath = Messages.getString("quick.start.select.interface.language.html");
			interfaceLanguagePage.setVisible(true);
			interfaceLanguagePage.setManaged(true);
			break;
			
		case LANGUAGES_PAGE:
			srcLangName = MinorityTranslateModel.wikis().getLangName(MinorityTranslateModel.preferences().languages().findLastSrcLangCode());
			dstLangName = MinorityTranslateModel.wikis().getLangName(MinorityTranslateModel.preferences().languages().findLastDstLangCode());

			block = true;
			if (srcLangName != null) srcLanguageEdit.setText(srcLangName);
			if (dstLangName != null) dstLanguageEdit.setText(dstLangName);
			block = false;
			
			pagePath = Messages.getString("quick.start.languages.html");
			languagesPage.setVisible(true);
			languagesPage.setManaged(true);
			break;

		case ADDING_ARTICLES_PAGE:
			pagePath = Messages.getString("quick.start.adding.articles.html");
			break;
    			
    	case AUTOCOMPLETE_PAGE:
    		pagePath = Messages.getString("quick.start.translate.features.html");
    		break;
        		
        case TRANSLATING_ARTICLES_PAGE:
        	pagePath = Messages.getString("quick.start.translate.extra.features.html");
        	break;

		case SESSIONS_PAGE:
			pagePath = Messages.getString("quick.start.sessions.html");
			break;

		case UPLOADING_ARTICLES_PAGE:
			pagePath = Messages.getString("quick.start.uploading.articles.html");
			break;
			
		case TEXT_CORPUS_PAGE:
			pagePath = Messages.getString("quick.start.text.corpus.html");
			
			srcLangCode = MinorityTranslateModel.preferences().languages().findLastSrcLangCode();
			dstLangCode = MinorityTranslateModel.preferences().languages().findLastDstLangCode();

			if (srcLangCode != null) {
				Proficiency prof = MinorityTranslateModel.preferences().corpus().getProficiency(srcLangCode);
				if (prof == null) prof = Proficiency.UNSPECIFIED;
				sourceProficiencyComboBox.getSelectionModel().select(prof.ordinal());
				srcLangLabel.setText(Messages.getString("quick.start.corpus.language.proficiency").replace("#language", MinorityTranslateModel.wikis().getLangName(srcLangCode)));
			} else {
				sourceProficiencyComboBox.setVisible(false);
				sourceProficiencyComboBox.setManaged(false);
				srcLangLabel.setVisible(false);
				srcLangLabel.setManaged(false);
			}

			if (dstLangCode != null) {
				Proficiency prof = MinorityTranslateModel.preferences().corpus().getProficiency(dstLangCode);
				if (prof == null) prof = Proficiency.UNSPECIFIED;
				destinationProficiencyComboBox.getSelectionModel().select(prof.ordinal());
				dstLangLabel.setText(Messages.getString("quick.start.corpus.language.proficiency").replace("#language", MinorityTranslateModel.wikis().getLangName(dstLangCode)));
			} else {
				destinationProficiencyComboBox.setVisible(false);
				destinationProficiencyComboBox.setManaged(false);
				dstLangLabel.setVisible(false);
				dstLangLabel.setManaged(false);
			}

			textCorpusPage.setVisible(true);
			textCorpusPage.setManaged(true);
			
			break;

		default:
			pagePath = Messages.getString("quick.start.empty.html");
			break;
		}
		
		// load page
		try {
			URL location = getClass().getResource(PAGES_PATH);
			String content = FileUtil.readFromJar(pagePath);
			content = content.replace("\"" + PAGES_PATH, "\"" + location.toExternalForm());
			htmlView.getEngine().loadContent(content);
		} catch (Exception e) {
			htmlView.getEngine().loadContent("Failed to load content for " + pagePath + "!");
			e.printStackTrace();
		}
	 }
	
	@Override
	protected void apply(String pageName)
	 {
		switch (pageName) {
		case INTERFACE_LANGUAGE_PAGE:
			String langName = languagesCombobox.getSelectionModel().getSelectedItem();
			String langCode = MinorityTranslateModel.wikis().getLangCode(langName);
			
			DisplayPreferences preferences = new DisplayPreferences(MinorityTranslateModel.preferences().display());
			String oldLangCode = preferences.getGUILangCode();
			preferences.setGuiLangCode(langCode);
			
			MinorityTranslateModel.notifier().sendNotification(Notifications.PREFERENCES_CHANGE_PROGRAM, preferences);
			if (!oldLangCode.equals(langCode)) {
				languagesCombobox.getScene().getWindow().hide();
				MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.SHOW_QUICK_START);
			}
			
			break;

		case LANGUAGES_PAGE:
			String srcLangCode = getLangCode(srcLanguageEdit.getText());
			String dstLangCode = getLangCode(dstLanguageEdit.getText());
			if (srcLangCode != null || dstLangCode != null) {
				LanguagesPreferences languagePreferences = new LanguagesPreferences(MinorityTranslateModel.preferences().languages());
				
				if (srcLangCode != null) {
					languagePreferences = languagePreferences.withLangCode(srcLangCode, Display.SOURCE);
				}
				if (dstLangCode != null) {
					languagePreferences = languagePreferences.withLangCode(dstLangCode, Display.DESTINATION);
				}
				MinorityTranslateModel.notifier().sendNotification(Notifications.PREFERENCES_CHANGE_LANGUAGES, languagePreferences);
			}
			
			break;

		case TEXT_CORPUS_PAGE:
			
			CorpusPreferences corpusPrefereces = new CorpusPreferences(MinorityTranslateModel.preferences().corpus());
			
			String srcProfLangCode = MinorityTranslateModel.preferences().languages().findLastSrcLangCode();
			if (srcProfLangCode != null) {
				Proficiency srcProf = Proficiency.values()[sourceProficiencyComboBox.getSelectionModel().getSelectedIndex()];
				corpusPrefereces = corpusPrefereces.withProficiency(srcProfLangCode, srcProf);
			}
			
			String dstProfLangCode = MinorityTranslateModel.preferences().languages().findLastDstLangCode();
			if (dstProfLangCode != null) {
				Proficiency dstProf = Proficiency.values()[destinationProficiencyComboBox.getSelectionModel().getSelectedIndex()];
				corpusPrefereces = corpusPrefereces.withProficiency(dstProfLangCode, dstProf);
			}
			
			corpusPrefereces = corpusPrefereces.withCollect(collectCorpusCheckbox.isSelected());
			
			MinorityTranslateModel.notifier().sendNotification(Notifications.PREFERENCES_CHANGE_CORPUS, corpusPrefereces);
			break;
			
		case EMPTY_PAGE:
			break;

		default:
			break;
		}	
	 }
	
	
	// HELPERS
	private String getLangCode(String text)
	 {
		String langCode = MinorityTranslateModel.wikis().getLangCode(text);
		if (langCode != null) return langCode;
		if (MinorityTranslateModel.wikis().isLangCode(text)) return text;
		return null;
	 }
	
}
