package ee.translate.keeleleek.mtapplication.view.pages;

import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.model.preferences.LookupsPreferences;


public abstract class LookupsPageMediator extends Mediator implements PreferencesPage<LookupsPreferences> {

	public final static String NAME = "{E1DDB052-CAC0-41B7-AC07-1B363F37EB11}";
	
	
	// INIT
	public LookupsPageMediator() {
		super(NAME, null);
	}

	@Override
	public void onRegister() {
		
	}

	
}
