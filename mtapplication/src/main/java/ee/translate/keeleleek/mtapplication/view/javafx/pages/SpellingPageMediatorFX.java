package ee.translate.keeleleek.mtapplication.view.javafx.pages;

import java.util.List;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.preferences.SpellingPreferences;
import ee.translate.keeleleek.mtapplication.view.javafx.fxemelents.PluginFX;
import ee.translate.keeleleek.mtapplication.view.pages.SpellingPageMediator;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.util.Callback;


public class SpellingPageMediatorFX extends SpellingPageMediator {

	@FXML
	private TableView<PluginFX> pluginTable;
	@FXML
	private TableColumn<PluginFX, String> pluginNameColumn;
	@FXML
	private TableColumn<PluginFX, String> pluginVersionColumn;

	
	/* ******************
	 *                  *
	 *  Initialization  *
	 *                  *
	 ****************** */
	@Override
	public void onRegister()
	 {
		super.onRegister();
		
	 }

	@FXML
	protected void initialize()
	 {
		pluginNameColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<PluginFX,String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(CellDataFeatures<PluginFX, String> insert) {
				return insert.getValue().name;
			}
		});
		
		pluginVersionColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<PluginFX,String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(CellDataFeatures<PluginFX, String> insert) {
				return insert.getValue().version;
			}
		});
	 }

	
	
	/* ******************
	 *                  *
	 *       Page       *
	 *                  *
	 ****************** */
	@Override
	public void open(SpellingPreferences preferences)
	 {
		String langCode = MinorityTranslateModel.preferences().display().getGUILangCode();
		List<String> pluginNames = MinorityTranslateModel.speller().getValidPluginNames(langCode);
		for (String name : pluginNames) {
			String version = MinorityTranslateModel.speller().getPlugin(langCode, name).getSpellerVersion().toString();
			pluginTable.getItems().add(new PluginFX(name, version));
		}
	 }
	
	@Override
	public SpellingPreferences close()
	 {
		return new SpellingPreferences();
	 }

	
}
