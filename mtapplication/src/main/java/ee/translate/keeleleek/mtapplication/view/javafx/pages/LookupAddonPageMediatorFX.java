package ee.translate.keeleleek.mtapplication.view.javafx.pages;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.common.requests.LookupRequest;
import ee.translate.keeleleek.mtapplication.common.requests.LookupRequest.LookupResponse;
import ee.translate.keeleleek.mtapplication.view.pages.LookupAddonPageMediator;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.web.WebView;


public class LookupAddonPageMediatorFX extends LookupAddonPageMediator {
	

	@FXML
	private ComboBox<String> lookupSelect;
	@FXML
	private TextField searchEdit;
	@FXML
	private Button searchButton;
	@FXML
	private WebView foundView;
	
	
	// INIT
	@Override
	public void onRegister()
	 {
		super.onRegister();
		
	 }

	@FXML
	protected void initialize()
	 {
		
	 }

	
	// IMPLEMENT
	protected void changeLookups(java.util.List<String> names)
	 {
		String selected = lookupSelect.getSelectionModel().getSelectedItem();
		
		lookupSelect.getItems().clear();
		lookupSelect.getItems().addAll(names);
		
		if (selected != null) lookupSelect.getSelectionModel().select(selected);
		if (lookupSelect.getSelectionModel().getSelectedIndex() == -1) lookupSelect.getSelectionModel().select(0);
		
		searchButton.setDisable(names.isEmpty());
	 };
	
	protected void searchFound(LookupResponse response)
	 {
		if (!searchEdit.getText().equals(response.getSearch())) return;
		if (lookupSelect.getSelectionModel().getSelectedItem() == null) return;
		if (!lookupSelect.getSelectionModel().getSelectedItem().equals(response.getName())) return;
		foundView.getEngine().loadContent(response.getFound());
	 };
	
	
	// PAGE
	@Override
	public void open()
	 {
		
	 }
	
	@Override
	public void close()
	 {
		
	 }

	
	// EVENTS
	@FXML
	private void onSearchClick()
	 {
		if (searchEdit.getText().isEmpty() || lookupSelect.getSelectionModel().getSelectedIndex() == -1) return;
		sendNotification(Notifications.REQUEST, new LookupRequest(NAME, lookupSelect.getSelectionModel().getSelectedItem(), searchEdit.getText()));
	 }
	
}
