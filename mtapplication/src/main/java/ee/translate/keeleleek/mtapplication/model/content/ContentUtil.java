package ee.translate.keeleleek.mtapplication.model.content;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.filters.Filter;

public class ContentUtil {

	public static <T extends WikiReference>  Set<String> findLangCodes(HashSet<T> requests)
	 {
		HashSet<String> result = new HashSet<>();
		
		Iterator<T> it = requests.iterator();
		while(it.hasNext()) {
	    	WikiReference request = it.next();
	    	result.add(request.getLangCode());
		}
		
		return result;
	 }
	
	public static Collection<WikiReference> takeRequests(String langCode, HashSet<WikiReference> requests, int limit)
	 {
		HashSet<WikiReference> result = new HashSet<>();
		
		Iterator<WikiReference> it = requests.iterator();
		while(it.hasNext()) {
			if (result.size() >= limit) break;
	    	WikiReference request = it.next();
	    	if (!request.getLangCode().equals(langCode)) continue;
	    	result.add(request);
	    	it.remove();
		}
		
		return result;
	 }
	
	public static void filterRequestLangCodes(HashSet<WikiReference> requests, String[] langCodesFilter)
	 {
		Iterator<WikiReference> it = requests.iterator();
		while(it.hasNext()) {
	    	WikiReference request = it.next();
	    	if (!containsLangCode(langCodesFilter, request.getLangCode())) it.remove();
		}
	 }
	
	private static boolean containsLangCode(String[] langCodes, String langCode) {
		for (int i = 0; i < langCodes.length; i++) {
			if (langCodes[i].equals(langCodes)) return true;
		}
		return false;
	}

	public static Collection<String> takeStringRequests(HashSet<String> requests, int limit)
	 {
		HashSet<String> result = new HashSet<>();
		
		Iterator<String> it = requests.iterator();
		while(it.hasNext()) {
			if (result.size() >= limit) break;
			String request = it.next();
	    	result.add(request);
	    	it.remove();
		}
		
		return result;
	 }

	public static Collection<WikiSubReference> takeSubrequests(String langCode, HashSet<WikiSubReference> requests, int limit)
	 {
		HashSet<WikiSubReference> result = new HashSet<>();
		
		Iterator<WikiSubReference> it = requests.iterator();
		while(it.hasNext()) {
			if (result.size() >= limit) break;
			WikiSubReference request = it.next();
	    	if (!request.getLangCode().equals(langCode)) continue;
	    	result.add(request);
	    	it.remove();
		}
		
		return result;
	 }

	public static WikiSubReference takeSubrequest(String langCode, HashSet<WikiSubReference> requests)
	 {
		Iterator<WikiSubReference> it = requests.iterator();
		while(it.hasNext()) {
			WikiSubReference request = it.next();
	    	if (request.getLangCode().equals(langCode)) {
		    	it.remove();
	    		return request;
	    	}
		}
		
		return null;
	 }
	

	public static List<String> extractTitles(Collection<WikiReference> requests)
	 {
		ArrayList<String> result = new ArrayList<>();
		for (WikiReference request : requests) {
			result.add(request.getNamespacedWikiTitle());
		}
		return result;
	 }
	
	public static List<String> extractSubTitles(Collection<WikiSubReference> requests)
	 {
		ArrayList<String> result = new ArrayList<>();
		for (WikiReference request : requests) {
			result.add(request.getWikiTitle());
		}
		return result;
	 }

	public static Collection<WikiReference> extractAllArticleRequests(HashMap<String, String> langlinks, String[] langCodes) {
		ArrayList<WikiReference> result = new ArrayList<>();
		
		for (String langCode : langCodes) {
			String title = langlinks.get(langCode);
			result.add(new WikiReference(langCode, Namespace.ARTICLE, title));
		}
		
		return result;
	}

	public static Collection<WikiReference> extractAllArticleRequests(HashMap<String, String> langlinks, Collection<String> langCodes) {
		ArrayList<WikiReference> result = new ArrayList<>();
		
		for (String langCode : langCodes) {
			String title = langlinks.get(langCode);
			result.add(new WikiReference(langCode, Namespace.ARTICLE, title));
		}
		
		return result;
	}
	

	public static Queue<MinorityArticle> filterArticles(ContentProxy content, MinorityArticle.Status status)
	 {
		Queue<MinorityArticle> result = new LinkedList<>();
		
		Collection<MinorityArticle> articles = content.getArticles();
		for (MinorityArticle article : articles) {
			if (article.getStatus() == status) result.add(article);
		}
		
		return result;
	 }
	
	public static MinorityArticle findArticle(ContentProxy content, MinorityArticle.TitleStatus status)
	 {
		Collection<MinorityArticle> articles = content.getArticles();
		for (MinorityArticle article : articles) {
			if (article.getTitleStatus() == status) return article;
		}
		
		return null;
	 }

	public static Queue<MinorityArticle> filterQidSortArticles(ContentProxy content, MinorityArticle.Status status)
	 {
		Queue<MinorityArticle> result = new LinkedList<>();
		
		List<String> qids = content.getQids();
		List<String> langCodes = MinorityTranslateModel.preferences().languages().getAllLangCodes();
		
		for (String qid : qids) {
			for (String langCode : langCodes) {
				
				Reference ref = new Reference(qid, langCode);
				MinorityArticle article = content.getArticle(ref);
				if (article != null && article.getStatus() == status) result.add(article);
				
			}
		}
		
		return result;
	 }

	
	public static Queue<MinorityArticle> filterPreviewArticles(ContentProxy content)
	 {
		Queue<MinorityArticle> result = new LinkedList<>();
		
		Collection<MinorityArticle> articles = content.getArticles();
		for (MinorityArticle article : articles) {
			if (article.isRequestPreview()) result.add(article);
		}
		
		return result;
	 }

	public static Queue<MinorityArticle> filterFilteredPreviewArticles(ContentProxy content)
	 {
		Queue<MinorityArticle> result = new LinkedList<>();
		
		Collection<MinorityArticle> articles = content.getArticles();
		for (MinorityArticle article : articles) {
			if (article.isRequestFilteredPreview()) result.add(article);
		}
		
		return result;
	 }

	public static Set<WikiReference> extractOrigRequests(ContentProxy content)
	 {
		HashSet<WikiReference> result = new HashSet<>();
		
		Collection<MinorityArticle> articles = content.getArticles();
		for (MinorityArticle article : articles) {
			result.add(article.getOriginalWikiReference());
		}
		
		return result;
	 }

	public static Set<Reference> extractReferences(ContentProxy content, String qid)
	 {
		HashSet<Reference> result = new HashSet<>();
		
		Collection<Reference> references = content.getReferences();
		for (Reference reference : references) {
			if (reference.getQid().equals(qid)) result.add(reference);
		}
		
		return result;
	 }

	public static List<String> extractTitles(ContentProxy content, String langCode)
	 {
		List<String> result = new ArrayList<>();

		Collection<MinorityArticle> articles = content.getArticles();
		for (MinorityArticle article : articles) {
			if (article.getRef().getLangCode().endsWith(langCode)) result.add(article.getTitle());
		}
		
		return result;
	 }

	public static String filterText(String text)
	 {
		Filter[] filters = MinorityTranslateModel.preferences().getActiveFilters();
		for (Filter filter : filters) {
			text = filter.filter(text);
		}
		return text;
	 }

	public static String[] searchTitles(ContentProxy content, String search, String langCode)
	 {
		ArrayList<String> hits = new ArrayList<>();
		
		Collection<MinorityArticle> articles = content.getArticles();
		for (MinorityArticle article : articles) {
			if (article.getTitle().toLowerCase().startsWith(search) && article.getRef().getLangCode().equals(langCode)) hits.add(article.getTitle());
		}
		
		Collections.sort(hits);
		
		int count = 10;
		if (hits.size() < count) count = hits.size();
		
		String[] result = new String[count];
		
		for (int i = 0; i < result.length; i++) {
			result[i] = hits.get(i);
		}
		
		return result;
	 }

	public static int countLangCodes(ContentProxy content, List<String> langCodes)
	 {
		int count = 0;
		
		Collection<MinorityArticle> articles = content.getArticles();
		for (MinorityArticle article : articles) {
			for (String langCode : langCodes) {
				if (article.getRef().getLangCode().equals(langCode)) count++;
			}
		}
		
		return count;
	 }

	public static int countLangCodesDone(ContentProxy content, List<String> langCodes)
	 {
		int count = 0;
		
		Collection<MinorityArticle> articles = content.getArticles();
		for (MinorityArticle article : articles) {
			for (String langCode : langCodes) {
				if (article.getRef().getLangCode().equals(langCode)) count++;
			}
		}
		
		return count;
	 }

	public static List<MinorityArticle> collectDstArticles(String qid)
	 {
		ArrayList<MinorityArticle> result = new ArrayList<>();
		
		List<String> langCodes = MinorityTranslateModel.preferences().languages().filterDstLangCodes();
		
		Collection<MinorityArticle> articles = MinorityTranslateModel.content().getArticles();
		for (MinorityArticle article : articles) {
			if (article.getRef().getQid().equals(qid)) {
				for (String langCode : langCodes) {
					if (article.getRef().getLangCode().equals(langCode)) {
						result.add(article);
						break;
					}
				}
			}
		}
		
		return result;
	 }

	
	
}
