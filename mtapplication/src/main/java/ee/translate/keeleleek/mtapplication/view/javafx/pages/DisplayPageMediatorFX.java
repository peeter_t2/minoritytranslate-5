package ee.translate.keeleleek.mtapplication.view.javafx.pages;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.preferences.DisplayPreferences;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;
import ee.translate.keeleleek.mtapplication.view.pages.DisplayPageMediator;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;


public class DisplayPageMediatorFX extends DisplayPageMediator {

	@FXML
	private ComboBox<String> guiLanguageSelect;
	@FXML
	private ComboBox<String> fontSizeSelect;
	@FXML
	private CheckBox templatesFilterSelect;
	@FXML
	private CheckBox filesFilterSelect;
	@FXML
	private CheckBox introductionFilterSelect;
	@FXML
	private CheckBox referencesFilterSelect;
	
	private String[] guiLangCodes = Messages.getLangCodes();

	
	
	/* ******************
	 *                  *
	 *  Initialization  *
	 *                  *
	 ****************** */
	@Override
	public void onRegister()
	 {
		super.onRegister();
		
	 }

	@FXML
	protected void initialize()
	 {
		// interface languages
		String[] guiLangNames = collectGUILanguages();
		guiLanguageSelect.getItems().clear();
		for (int i = 0; i < guiLangNames.length; i++) {
			guiLanguageSelect.getItems().add(guiLangNames[i]);
		}
		
		// font size
		fontSizeSelect.getItems().addAll(DisplayPreferences.FONT_SIZES);
	 }

	
	
	/* ******************
	 *                  *
	 *       Page       *
	 *                  *
	 ****************** */
	@Override
	public void open(DisplayPreferences preferences)
	 {
		// interface language
		int selected = -1;
		for (int i = 0; i < guiLangCodes.length; i++) {
			if (guiLangCodes[i].equals(preferences.getGUILangCode())) selected = i;
		}
		if (selected != -1) guiLanguageSelect.getSelectionModel().select(selected);
		
		// font size
		fontSizeSelect.getSelectionModel().select(MinorityTranslateModel.preferences().getFontSize());
		
		// filters
		templatesFilterSelect.setSelected(preferences.isTemplatesFilter());
		introductionFilterSelect.setSelected(preferences.isIntroductionFilter());
		filesFilterSelect.setSelected(preferences.isFilesFilter());
		referencesFilterSelect.setSelected(preferences.isReferencesFilter());
	 }
	
	@Override
	public DisplayPreferences close()
	 {
		return new DisplayPreferences(
				templatesFilterSelect.isSelected(),
				filesFilterSelect.isSelected(),
				introductionFilterSelect.isSelected(),
				referencesFilterSelect.isSelected(),
				guiLangCodes[guiLanguageSelect.getSelectionModel().getSelectedIndex()],
				fontSizeSelect.getSelectionModel().getSelectedItem()
			);
	 }

	
	
	/* ******************
	 *                  *
	 *     Helpers      *
	 *                  *
	 ****************** */
	public String[] collectGUILanguages()
	 {
		String[] guiLangNames = new String[guiLangCodes.length];
		for (int i = 0; i < guiLangNames.length; i++) {
			String langName = MinorityTranslateModel.wikis().getLangName(guiLangCodes[i]);
			if (langName == null) langName = guiLangCodes[i];
			guiLangNames[i] = langName;
		}
		
		return guiLangNames;
	 }
	
	
}
