package ee.translate.keeleleek.mtapplication.model.autocomplete;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import ee.translate.keeleleek.mtapplication.model.content.Reference;

public class AutocompleteChoices {

	private Reference ref;
	private List<AutocompleteChoice> choises;
	
	
	public AutocompleteChoices(Reference ref) {
		this.ref = ref;
		choises = new ArrayList<AutocompleteChoice>();
	}

	
	public void addChoise(AutocompleteChoice choise) {
		choises.add(choise);
	}

	public void addChoises(AutocompleteChoice[] choises) {
		for (AutocompleteChoice choise : choises) {
			addChoise(choise);
		}
	}
	
	public void addChoises(Collection<AutocompleteChoice> choises) {
		for (AutocompleteChoice choise : choises) {
			addChoise(choise);
		}
	}
	
	
	public Reference getRef() {
		return ref;
	}
	
	public List<AutocompleteChoice> getChoises() {
		return choises;
	}
	
	
}
