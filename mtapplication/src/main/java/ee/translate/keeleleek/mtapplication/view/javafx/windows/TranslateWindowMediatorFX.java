package ee.translate.keeleleek.mtapplication.view.javafx.windows;

import java.util.ArrayList;
import java.util.List;

import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.view.elements.EditorMediator;
import ee.translate.keeleleek.mtapplication.view.javafx.MediatorLoaderFX;
import ee.translate.keeleleek.mtapplication.view.javafx.elements.ArticlesMediatorFX;
import ee.translate.keeleleek.mtapplication.view.javafx.elements.NotesMediatorFX;
import ee.translate.keeleleek.mtapplication.view.javafx.views.AlignViewMediatorFX;
import ee.translate.keeleleek.mtapplication.view.javafx.views.SingleViewMediatorFX;
import ee.translate.keeleleek.mtapplication.view.javafx.views.ViewMediatorFX;
import ee.translate.keeleleek.mtapplication.view.pages.FindAddonPageMediator;
import ee.translate.keeleleek.mtapplication.view.pages.LookupAddonPageMediator;
import ee.translate.keeleleek.mtapplication.view.views.ViewMediator.ViewMode;
import ee.translate.keeleleek.mtapplication.view.windows.TranslateWindowMediator;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.SplitMenuButton;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class TranslateWindowMediatorFX extends TranslateWindowMediator {

	@FXML
	protected SplitPane addonsSplitPane;

	@FXML
	protected MenuItem loginMenuItem;
	@FXML
	protected MenuItem logoutMenuItem;
	@FXML
	protected MenuItem nextArticleMenuItem;
	@FXML
	protected MenuItem previousArticleMenuItem;
	@FXML
	protected MenuItem menuExactToggleItem;
	@FXML
	protected CheckMenuItem uploadEnabledMenuItem;
	@FXML
	protected MenuItem uploadMenuItem;
	@FXML
	protected MenuItem addArticleMenuItem;
	@FXML
	protected MenuItem addCategoryMenuItem;
	@FXML
	protected MenuItem addLinksMenuItem;
	@FXML
	protected MenuItem listsMenuItem;
	@FXML
	protected CheckMenuItem filterTemplatesMenuItem;
	@FXML
	protected CheckMenuItem filterFilesMenuItem;
	@FXML
	protected CheckMenuItem filterIntroductionMenuItem;
	@FXML
	protected CheckMenuItem filterReferencesMenuItem;

	@FXML
	protected StackPane editorsPane;
	protected SplitPane verticalViewPane;
	protected SplitPane horizontalViewPane;
	protected HBox alignViewPane;
	protected Parent singleViewPane;
	
	@FXML
	protected Circle loginIndicatorCircle;

	@FXML
	protected ProgressBar progressBar;

	@FXML
	protected ToggleButton uploadButton;

	@FXML
	protected SplitMenuButton pullButton;
	@FXML
	protected ToggleButton spellCheckToggle;

	@FXML
	protected Button nextButton;
	@FXML
	protected Button previousButton;
	@FXML
	protected Button doneButton;

	@FXML
	private RadioMenuItem horizontalViewToggle;
	@FXML
	private RadioMenuItem verticalViewToggle;
	@FXML
	protected RadioMenuItem alignViewToggle;
	@FXML
	protected RadioMenuItem singleViewToggle;
	
	@FXML
	protected HBox wikisBox;
	@FXML
	protected Label tagLabel;
	@FXML
	protected Label countsLabel;

	@FXML
	protected ProgressIndicator spellCheckingProgress;
	@FXML
	protected Label spellCheckingProgressLabel;
	@FXML
	protected ProgressIndicator quererProgress;
	@FXML
	protected Label quererProgressLabel;
	@FXML
	protected ProgressIndicator downloaderProgress;
	@FXML
	protected Label downloaderProgressLabel;
	@FXML
	protected ProgressIndicator previewerProgress;
	@FXML
	protected Label previewerProgressLabel;
	@FXML
	protected ProgressIndicator uploaderProgress;
	@FXML
	protected Label uploaderProgressLabel;
	@FXML
	protected ProgressIndicator savingSessionProgress;
	@FXML
	protected Label savingSessionProgressLabel;
	@FXML
	protected ProgressIndicator pullingProgress;
	@FXML
	protected Label pullingProgressLabel;
	@FXML
	protected ProgressIndicator checkingTitleProgress;
	@FXML
	protected Label checkingTitleProgressLabel;
	

	@FXML
	protected Tab snippetsTab;
	@FXML
	protected Tab lookupTab;
	@FXML
	protected Tab findTab;
	@FXML
	protected ListView<Label> articleList;
	@FXML
	protected TextArea notesEdit;
	
	protected ArrayList<EditorMediator> upperTabs = new ArrayList<>();
	protected ArrayList<EditorMediator> lowerTabs = new ArrayList<>();
	
	
	// INIT
	@FXML
	public void initialize()
	 {
		List<String> pullPluginNames = MinorityTranslateModel.pull().getPluginNames(MinorityTranslateModel.preferences().getGUILangCode());
		
		pullButton.getItems().clear();

		boolean first = true;
		ToggleGroup toggleGroup = new ToggleGroup();
		for (String pluginName : pullPluginNames) {
			RadioMenuItem item = new RadioMenuItem(pluginName);
			if (first) {
				item.setSelected(true);
				first = false;
			}
			item.setToggleGroup(toggleGroup);
			pullButton.getItems().add(item);
		}
		
		// disable deselect
		horizontalViewToggle.getToggleGroup().selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
			@Override
			public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
				if (newValue == null) oldValue.setSelected(true);
			}
		});
	 };
	
	@Override
	public void onRegister()
	 {
		super.onRegister();
		
		// Snippets addon
		Mediator snippetsMediator = MediatorLoaderFX.loadSnippetsPaneMediator();
		snippetsTab.setContent((Node) snippetsMediator.getViewComponent());
		getFacade().registerMediator(snippetsMediator);
		
		// lookup addon
		lookupTab.selectedProperty().addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable obs) {
				if (lookupTab.getContent() != null) return;
				LookupAddonPageMediator lookupMediator = MediatorLoaderFX.loadLookupAddonPageMediator();
				lookupTab.setContent((Node) lookupMediator.getViewComponent());
				getFacade().registerMediator(lookupMediator);
				lookupMediator.open();
			}
		});

		// find addon
		findTab.selectedProperty().addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable obs) {
				if (findTab.getContent() != null) return;
				FindAddonPageMediator findMediator = MediatorLoaderFX.loadFindAddonPageMediator();
				findTab.setContent((Node) findMediator.getViewComponent());
				getFacade().registerMediator(findMediator);
				findMediator.open();
			}
		});
	 }
	
	@Override
	public Mediator createArticlesMediator() {
		return new ArticlesMediatorFX(articleList);
	}

	@Override
	public Mediator createNotesMediator() {
		return new NotesMediatorFX(notesEdit);
	}
	
	public void openView(ViewMode viewMode)
	 {
		switch (viewMode) {
		case SPLIT_VERTICAL:
			if (activeView != null && activeView == verticalView) break;

			if (horizontalView != null) { // close horizontal
				getFacade().removeMediator(horizontalView.getMediatorName());
				editorsPane.getChildren().remove(horizontalViewPane);
				horizontalView = null;
				horizontalViewPane = null;
			}

			if (alignView != null) { // close align
				getFacade().removeMediator(alignView.getMediatorName());
				editorsPane.getChildren().remove(alignViewPane);
				alignView = null;
				alignViewPane = null;
			}

			if (singleView != null) { // close single
				getFacade().removeMediator(singleView.getMediatorName());
				editorsPane.getChildren().remove(singleViewPane);
				singleView = null;
				singleViewPane = null;
			}
			
			if (verticalViewPane == null) { // open vertical
				verticalView = new ViewMediatorFX(viewMode);
				verticalViewPane = MediatorLoaderFX.loadSplitView(verticalView);
				editorsPane.getChildren().add(verticalViewPane);
				getFacade().registerMediator(verticalView);
			} else {
				verticalViewPane.requestLayout();
			}
			
			this.activeView = verticalView;
			this.verticalViewToggle.setSelected(true);
			break;
			
		case SPLIT_HORIZONTAL:
			if (activeView != null && activeView == horizontalView) break;

			if (verticalView != null) { // close vertical
				getFacade().removeMediator(verticalView.getMediatorName());
				editorsPane.getChildren().remove(verticalViewPane);
				verticalView = null;
				verticalViewPane = null;
			}

			if (alignView != null) { // close align
				getFacade().removeMediator(alignView.getMediatorName());
				editorsPane.getChildren().remove(alignViewPane);
				alignView = null;
				alignViewPane = null;
			}

			if (singleView != null) { // close single
				getFacade().removeMediator(singleView.getMediatorName());
				editorsPane.getChildren().remove(singleViewPane);
				singleView = null;
				singleViewPane = null;
			}
			
			if (horizontalViewPane == null) { // open horizontal
				horizontalView = new ViewMediatorFX(viewMode);
				horizontalViewPane = MediatorLoaderFX.loadSplitView(horizontalView);
				editorsPane.getChildren().add(horizontalViewPane);
				getFacade().registerMediator(horizontalView);
			} else {
				horizontalViewPane.requestLayout();
			}

			this.activeView = horizontalView;
			this.horizontalViewToggle.setSelected(true);
			break;
		
		case ALIGN:
			if (activeView != null && activeView == alignView) break;

			if (verticalView != null) { // close vertical
				getFacade().removeMediator(verticalView.getMediatorName());
				editorsPane.getChildren().remove(verticalViewPane);
				verticalView = null;
				verticalViewPane = null;
			}

			if (horizontalView != null) { // close horizontal
				getFacade().removeMediator(horizontalView.getMediatorName());
				editorsPane.getChildren().remove(horizontalViewPane);
				horizontalView = null;
				horizontalViewPane = null;
			}

			if (singleView != null) { // close single
				getFacade().removeMediator(singleView.getMediatorName());
				editorsPane.getChildren().remove(singleViewPane);
				singleView = null;
				singleViewPane = null;
			}
			
			if (alignViewPane == null) { // open align
				alignView = new AlignViewMediatorFX(viewMode);
				alignViewPane = MediatorLoaderFX.loadAlignView(alignView);
				editorsPane.getChildren().add(alignViewPane);
				getFacade().registerMediator(alignView);
			} else {
				alignViewPane.requestLayout();
			}

			this.activeView = alignView;
			this.alignViewToggle.setSelected(true);
			break;

		case SINGLE:
			if (activeView != null && activeView == singleView) break;

			if (verticalView != null) { // close vertical
				getFacade().removeMediator(verticalView.getMediatorName());
				editorsPane.getChildren().remove(verticalViewPane);
				verticalView = null;
				verticalViewPane = null;
			}

			if (horizontalView != null) { // close horizontal
				getFacade().removeMediator(horizontalView.getMediatorName());
				editorsPane.getChildren().remove(horizontalViewPane);
				horizontalView = null;
				horizontalViewPane = null;
			}

			if (alignView != null) { // close align
				getFacade().removeMediator(alignView.getMediatorName());
				editorsPane.getChildren().remove(alignViewPane);
				alignView = null;
				alignViewPane = null;
			}
			
			if (singleViewPane == null) { // open single
				singleView = new SingleViewMediatorFX(viewMode);
				singleViewPane = MediatorLoaderFX.loadSingleView(singleView);
				editorsPane.getChildren().add(singleViewPane);
				getFacade().registerMediator(singleView);
			} else {
				singleViewPane.requestLayout();
			}

			this.activeView = singleView;
			this.singleViewToggle.setSelected(true);
			break;

		default:
			break;
		}
	 }
	
	@Override
	public ViewMode getView() {
		return activeView.getViewMode();
	}
	

	// INHERIT (MENU CONTROLS)
	@Override
	protected void setMenuUploadEnabled(boolean enabled) {
		uploadEnabledMenuItem.setSelected(enabled);
	}
	
	@Override
	protected void disableMenuExactToggle(boolean disabled) {
		menuExactToggleItem.setDisable(disabled);
	}
	

	// INHERIT (CONTROLS)
	protected void setNavigationDisable(boolean disabled)
	 {
		nextButton.setDisable(disabled);
		nextArticleMenuItem.setDisable(disabled);
		
		previousButton.setDisable(disabled);
		previousArticleMenuItem.setDisable(disabled);
	 };


	@Override
	protected boolean isUpload() {
		return uploadButton.isSelected();
	}
	
	@Override
	protected void setUpload(boolean upload) {
		uploadButton.setSelected(upload);
	}

	@Override
	protected void setUploadDisable(boolean disabled) {
		uploadButton.setDisable(disabled);
		uploadMenuItem.setDisable(disabled);
	}

	@Override
	protected String getPullPluginName() {
		ObservableList<MenuItem> menu = pullButton.getItems();
		for (MenuItem menuItem : menu) {
			if (menuItem instanceof RadioMenuItem) {
				if (((RadioMenuItem) menuItem).isSelected()) return menuItem.getText();
			}
		}
		return null;
	}
	
	@Override
	protected void setPullDisable(boolean disabled) {
		pullButton.setDisable(disabled);
	}


	// INHERIT (DIVIDER)
	@Override
	public int getAddonsDivider() {
		return (int)Math.floor(addonsSplitPane.getDividerPositions()[0] * addonsSplitPane.getWidth());
	}
	
	@Override
	public void setAddonsDivider(int position) {
		if ((int)addonsSplitPane.getWidth() > 0.0) addonsSplitPane.setDividerPositions((double)position / addonsSplitPane.getWidth());
	}
	
	@Override
	public double getTranslateDivider() {
		return 0.5;
		// TODO return verticalViewPane.getDividerPositions()[0];
	}
	
	@Override
	public void setTranslateDivider(double position) {
		// TODO verticalViewPane.setDividerPositions(position);
	}
	
	
	// INHERIT (STATUS)
	protected void showWikis(List<String> langCodes)
	 {
		ObservableList<Node> children = wikisBox.getChildren();
		children.clear();
		for (final String langCode : langCodes) {
			Hyperlink hyperlink = new Hyperlink(langCode);
			hyperlink.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					onOpenWiki(langCode);
				}
			});
			children.add(hyperlink);
		}
	 }
	
	@Override
	protected void showTag(String tag) {
		tagLabel.setText(tag);
	}
	
	@Override
	protected void showCount(String count) {
		countsLabel.setText(count);
	}
	
	
	@Override
	protected void setLoginIndicator(java.awt.Color colour, String tooltip) {
		loginIndicatorCircle.setFill(new Color((double)colour.getRed()/255.0, (double)colour.getGreen()/255.0, (double)colour.getBlue()/255.0, (double)colour.getAlpha()/255.0));
		Tooltip.install(loginIndicatorCircle, new Tooltip(tooltip));
	}
	

	protected void setProgressVisible(boolean visible) {
		progressBar.setVisible(visible);
	}
	
	protected void setProgress(double value) {
		progressBar.setProgress(value);
	}


	@Override
	protected void showSpellCheckerBusy(boolean busy) {
		spellCheckingProgress.setVisible(busy);
		spellCheckingProgress.setManaged(busy);
		spellCheckingProgressLabel.setVisible(busy);
		spellCheckingProgressLabel.setManaged(busy);
	}

	@Override
	protected void showQueuerBusy(boolean busy) {
		quererProgress.setVisible(busy);
		quererProgress.setManaged(busy);
		quererProgressLabel.setVisible(busy);
		quererProgressLabel.setManaged(busy);
	}

	@Override
	protected void showDownloaderBusy(boolean busy) {
		downloaderProgress.setVisible(busy);
		downloaderProgress.setManaged(busy);
		downloaderProgressLabel.setVisible(busy);
		downloaderProgressLabel.setManaged(busy);
	}

	@Override
	protected void showPreviewerBusy(boolean busy) {
		previewerProgress.setVisible(busy);
		previewerProgress.setManaged(busy);
		previewerProgressLabel.setVisible(busy);
		previewerProgressLabel.setManaged(busy);
	}

	@Override
	protected void showUploaderBusy(boolean busy) {
		uploaderProgress.setVisible(busy);
		uploaderProgress.setManaged(busy);
		uploaderProgressLabel.setVisible(busy);
		uploaderProgressLabel.setManaged(busy);
	}

	@Override
	protected void showSessionSaveBusy(boolean busy) {
		savingSessionProgress.setVisible(busy);
		savingSessionProgress.setManaged(busy);
		savingSessionProgressLabel.setVisible(busy);
		savingSessionProgressLabel.setManaged(busy);
	}

	@Override
	protected void showPullingBusy(boolean busy) {
		pullingProgress.setVisible(busy);
		pullingProgress.setManaged(busy);
		pullingProgressLabel.setVisible(busy);
		pullingProgressLabel.setManaged(busy);
	}
	
	@Override
	protected void showCheckingTitleBusy(boolean busy) {
		checkingTitleProgress.setVisible(busy);
		checkingTitleProgress.setManaged(busy);
		checkingTitleProgressLabel.setVisible(busy);
		checkingTitleProgressLabel.setManaged(busy);
	}

	// EVENTS
	@FXML
	public void onNextClick() {
		super.onNext();
	}

	@FXML
	public void onPreviousClick() {
		super.onPrevious();
	}
	
	
	@FXML
	public void onMenuSessionNewClick() {
		super.onSessionNew();
	}

	@FXML
	public void onMenuSessionOpenClick() {
		super.onSessionOpen();
	}

	@FXML
	public void onMenuSessionSaveClick() {
		super.onSessionSave();
	}

	@FXML
	public void onMenuSessionSaveAsClick() {
		super.onSessionSaveAs();
	}
	
	@FXML
	public void onMenuPreferencesClick() {
		super.onPreferences();
	}
	
	@FXML
	public void onMenuLoginClick() {
		super.onLogin();
	}
	
	@FXML
	public void onMenuLogoutClick() {
		super.onLogout();
	}

	@FXML
	public void onMenuQuitClick() {
		super.onQuit();
	}

	@FXML
	public void onMenuUploadClick() {
		super.onUpload();
	}

	@FXML
	public void onMenuUploadEnabled() {
		super.onUploadEnable(uploadEnabledMenuItem.isSelected());
	}
	
	@FXML
	public void onMenuAddCategoryClick() {
		super.onAddCategory();
	}

	@FXML
	public void onMenuAddArticleClick() {
		super.onAddArticle();
	}

	@FXML
	public void onMenuAddLinksClick() {
		super.onAddLinks();
	}

	@FXML
	public void onMenuAddContentClick() {
		super.onAddContent();
	}
	
	
	@FXML
	public void onMenuStopDownloadingClick() {
		super.onStopDownloading();
	}
	
	@FXML
	public void onMenuListsClick() {
		super.onLists();
	}
	
	@FXML
	public void onMenuRemoveArticleClick() {
		super.onRemoveArticle();
	}

	@FXML
	public void onMenuRemoveSelectedArticleClick() {
		super.onRemoveSelectedArticle();
	}

	@FXML
	public void onMenuAboutClick() {
		super.onAbout();
	}

	@FXML
	public void onMenuManualClick() {
		super.onManual();
	}

	@FXML
	public void onMenuResetQuickStartClick() {
		super.onResetQuickStart();
	}
	
	
	// EVENTS (MENU)
	public void onMenuTogglePreviewClick() {
		super.onTogglePreview();
	}
	
	@FXML
	public void onMenuExactToggleClick() {
		super.onToggleExact();
	}
	
	
	// EVENTS (CONTROL)
	@FXML
	public void onMenuNextArticleClick() {
		super.onNextArticle();
	}

	@FXML
	public void onMenuPreviousArticleClick() {
		super.onPreviousArticle();
	}


	@FXML
	public void onUploadToggle()
	 {
		super.onUpload(uploadButton.isSelected());
	 }

	@FXML
	public void onPullClick()
	 {
		super.onPull();
	 }

	@FXML
	public void onTagClick()
	 {
		super.onTag();
	 }

	
	// EVENTS (VIEWS)
	@FXML
	public void onHorizontalViewClick(ActionEvent event)
	 {
		if (!horizontalViewToggle.isSelected()) return;
		
		horizontalViewToggle.setSelected(false);
		verticalViewToggle.setSelected(false);
		alignViewToggle.setSelected(false);
		singleViewToggle.setSelected(false);
		
		super.onHorizontalView();
	 }
	
	@FXML
	public void onVerticalViewClick()
	 {
		if (!verticalViewToggle.isSelected()) return;
		
		horizontalViewToggle.setSelected(false);
		alignViewToggle.setSelected(false);
		singleViewToggle.setSelected(false);
		
		super.onVerticalView();
	 }

	@FXML
	public void onAlignViewClick()
	 {
		if (!alignViewToggle.isSelected()) return;
		
		horizontalViewToggle.setSelected(false);
		verticalViewToggle.setSelected(false);
		singleViewToggle.setSelected(false);
		
		super.onAlignView();
	 }

	@FXML
	public void onSingleViewClick()
	 {
		if (!singleViewToggle.isSelected()) return;
		
		horizontalViewToggle.setSelected(false);
		verticalViewToggle.setSelected(false);
		alignViewToggle.setSelected(false);
		
		super.onSingleView();
	 }
	
	
}
