package ee.translate.keeleleek.mtapplication.model.content;

import java.util.Collection;
import java.util.HashSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.common.requests.ContentRequest;
import ee.translate.keeleleek.mtapplication.common.requests.ContentSubRequest;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.Status;
import ee.translate.keeleleek.mtapplication.model.wikis.WikisProxy;

public class ContentQueuerProxy extends ContentExpanderProxy implements Runnable {

	public final static String NAME = "{9D2583E3-0BF2-4DC2-B4C1-A733476FEE67}";
	
	private static Logger LOGGER = LoggerFactory.getLogger(ContentQueuerProxy.class);
	
	public final static long TIMEOUT = 250*0 + 1000;
	
	private boolean busy = false;
	private boolean run = false;
	
	
	// INIT
	public ContentQueuerProxy() {
		super(NAME);
	}

	@Override
	public void onRegister()
	 {
		run = true;
		
		workLangCodes();
		
		startThread();
	 }
	
	@Override
	public void onRemove() {
		run = false;
	}
	
	private void startThread()
	 {
		Thread thread = new Thread(this);
		thread.setDaemon(true);
		thread.start();
	 }
	
	
	// REQUEST
	public void requestArticle(String langCode, String title)
	 {
		String wikiTitle = WikisProxy.wikiTitle(title, langCode);
		articles.add(new WikiReference(langCode, Namespace.ARTICLE, wikiTitle));
	 }
	
	public void requestCategory(String langCode, String title)
	 {
		String wikiTitle = WikisProxy.wikiTitle(title, langCode);
		categories.add(new WikiSubReference(langCode, Namespace.CATEGORY, wikiTitle));
	 }
	
	public void requestContent(ContentRequest request)
	 {
		String title = request.getTitle();
		String langCode = request.getLangCode();
		Namespace namespace = Namespace.find(request.getNamespace());
		
		if (title == null || title.isEmpty()) return;
		if (!MinorityTranslateModel.wikis().isLangCode(langCode)) return;
		if (namespace == null) return;

		String wikiTitle = WikisProxy.wikiTitle(title, langCode);
		WikiReference ref = new WikiReference(langCode, namespace, wikiTitle);
		
		articles.add(ref);
		
		if (request instanceof ContentSubRequest) {
			
			ContentSubRequest subrequest = (ContentSubRequest) request;
			
			if (subrequest.isCategory() && subrequest.isExpand() && subrequest.isAdd()) {
				WikiSubReference subref = new WikiSubReference(langCode, namespace, wikiTitle, subrequest.isAddArticles(), subrequest.isAddCategories());
				categories.add(subref);
			}
			
		}
	 }
	

	public void requestExpanded(WikiRequest request)
	 {
		expanded.add(request);
	 }
	
	
	// WORK
	@Override
	public void run()
	 {
		try {
			while (run) {
				
				lookBusy();
				
				work();
				
				lookBusy();
				
				Thread.sleep(TIMEOUT);
			}
		}
		catch (InterruptedException e) {
			LOGGER.error("Thread sleep failure!", e);
		}
		catch (IllegalStateException e) {
			LOGGER.warn("Failed to queue content: " + e.getMessage() + "!");
			startThread();
		}
	 }
	
	
	@Override
	protected void workConclude(HashSet<WikiRequest> expanded)
	 {
		int qguard = guard;
		
		for (WikiRequest expand : expanded) {
			
			String qid = expand.getQid();
			
			Collection<WikiReference> requests = expand.getRequests();
			for (WikiReference request : requests) {
				
				// adding queue element
				FullRequest fullref = new FullRequest(qid, request.getLangCode(), request.getNamespace(), request.getWikiTitle());
				
				// check article
				Reference ref = fullref.createReference();
				if (MinorityTranslateModel.content().hasArticle(ref)) continue;
				
				if (qguard == guard) {

					// creating article and request download
					MinorityTranslateModel.content().createArticle(expand.getOriginal(), ref, fullref.getWikiTitle());
					MinorityTranslateModel.content().changeStatus(ref, Status.DOWNLOAD_REQUESTED);
					
					// selecting first
					if (MinorityTranslateModel.content().getSelectedQid() == null) {
//						MinorityTranslateModel.content().changeSelectedQid(fullref.getQid());
//						MinorityTranslateModel.content().changeSelectedLangCode(MinorityTranslateModel.preferences().getMainLangCode());
					}
					
				}
			}
			
			// meta
			MinorityTranslateModel.content().updateQidMeta(expand.getQidMeta());
			
		}
	 }
	
	private void lookBusy()
	 {
		boolean busy = articles.size() > 0 || categories.size() > 0 || redirected.size() > 0 || qids.size() > 0 || expanded.size() > 0;

		if (this.busy == busy) return;
		
		this.busy = busy;
		
		MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.ENGINE_QUERER_BUSY, busy);
	 }
	
	
	
}
