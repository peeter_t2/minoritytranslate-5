package ee.translate.keeleleek.mtapplication.view.dialogs;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.mediator.Mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.preferences.CollectPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.ContentAssistPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.ContentPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.DisplayPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.InsertsPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.LookupsPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.PullingPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.ReplacePreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.SymbolsPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.TemplateMappingPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.TranslatePreferences;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;
import ee.translate.keeleleek.mtapplication.view.pages.CollectPageMediator;
import ee.translate.keeleleek.mtapplication.view.pages.ContentAssistPageMediator;
import ee.translate.keeleleek.mtapplication.view.pages.ContentPageMediator;
import ee.translate.keeleleek.mtapplication.view.pages.DisplayPageMediator;
import ee.translate.keeleleek.mtapplication.view.pages.InsertsPageMediator;
import ee.translate.keeleleek.mtapplication.view.pages.LookupsPageMediator;
import ee.translate.keeleleek.mtapplication.view.pages.PullingPageMediator;
import ee.translate.keeleleek.mtapplication.view.pages.ReplacePageMediator;
import ee.translate.keeleleek.mtapplication.view.pages.SpellingPageMediator;
import ee.translate.keeleleek.mtapplication.view.pages.SymbolsPageMediator;
import ee.translate.keeleleek.mtapplication.view.pages.TemplateMappingPageMediator;
import ee.translate.keeleleek.mtapplication.view.pages.TranslatePageMediator;


public abstract class PreferencesDialogMediator extends Mediator implements ConfirmDeclineDialog {

	final public static String NAME = "{ABC1B283-5CFF-4D88-8522-65D3E84E5CFD}";
	
	protected static Logger LOGGER = LoggerFactory.getLogger(PreferencesDialogMediator.class);
	
	protected TranslatePageMediator translatePageMediator = null;
	protected DisplayPageMediator displayPageMediator = null;
	protected ContentPageMediator contentPageMediator = null;
	protected CollectPageMediator collectPageMediator = null;
	protected ReplacePageMediator replacePageMediator = null;
	protected PullingPageMediator pullingPageMediator = null;
	protected SpellingPageMediator spellingPageMediator = null;
	protected TemplateMappingPageMediator templateMappingMediator = null;
	protected ContentAssistPageMediator contentAssistMediator = null;
	protected InsertsPageMediator templatesMediator = null;
	protected SymbolsPageMediator symbolsMediator = null;
	protected LookupsPageMediator lookupsMediator = null;
	
	protected boolean resetting = false;
	protected boolean manual = false;
	
	protected String[] guiLangCodes = Messages.getLangCodes();
	protected String[] guiLangNames = collectGUILanguages();;
	
	
	// INIT
	public PreferencesDialogMediator() {
		super(NAME, null);
	}
	
	@Override
	public void onRegister() {
		
	}
	

	// NOTIFICATIONS
	@Override
	public String[] listNotificationInterests() {
		return new String[]
		 {
			Notifications.WINDOW_PREFERENCES_PREPARE,
			Notifications.SESSION_LOADED,
			Notifications.CONNECTION_STATUS_CHANGED,
			Notifications.DOWNLOADER_STARTED,
			Notifications.DOWNLOADER_ENDED
		 };
	}
	
	@Override
	public void handleNotification(INotification notification)
	 {
		switch (notification.getName()) {
		case Notifications.WINDOW_PREFERENCES_PREPARE:
			break;
				
		 default:
			break;
		}
	 }
	
	
	// INHERIT (INIT)
	protected abstract void openTemplateMapping(TemplateMappingPreferences preferences);
	protected abstract TemplateMappingPreferences closeTemplateMapping();

	protected abstract void openContentAssist(ContentAssistPreferences preferences);
	protected abstract ContentAssistPreferences closeContentAssist();

	protected abstract void openInserts(InsertsPreferences preferences);
	protected abstract InsertsPreferences closeInserts();

	protected abstract void openSymbols(SymbolsPreferences prefernces);
	protected abstract SymbolsPreferences closeSymbols();

	protected abstract void openLookups(LookupsPreferences prefernces);
	protected abstract LookupsPreferences closeLookups();

	
	// INHERIT (CONFIRM DECLINE DIALOG)
	@Override
	public void focus()
	 {
		
	 }
	
	@Override
    public void confirm()
	 {
		String guiLangCode = MinorityTranslateModel.preferences().display().getGUILangCode();
		String fontSize = MinorityTranslateModel.preferences().display().getFontSize();
		
		// close pages
		if (translatePageMediator!= null) {
			TranslatePreferences preferences = translatePageMediator.close();
			translatePageMediator = null;
			sendNotification(Notifications.PREFERENCES_CHANGE, preferences);
		}

		if (displayPageMediator!= null) {
			DisplayPreferences preferences = displayPageMediator.close();
			displayPageMediator = null;
			sendNotification(Notifications.PREFERENCES_CHANGE, preferences);
		}

		if (contentPageMediator!= null) {
			ContentPreferences preferences = contentPageMediator.close();
			contentPageMediator = null;
			sendNotification(Notifications.PREFERENCES_CHANGE, preferences);
		}
		
		if (collectPageMediator!= null) {
			CollectPreferences preferences = collectPageMediator.close();
			collectPageMediator = null;
			sendNotification(Notifications.PREFERENCES_CHANGE, preferences);
		}

		if (replacePageMediator!= null) {
			ReplacePreferences preferences = replacePageMediator.close();
			replacePageMediator = null;
			sendNotification(Notifications.PREFERENCES_CHANGE, preferences);
		}

		if (pullingPageMediator!= null) {
			PullingPreferences preferences = pullingPageMediator.close();
			pullingPageMediator = null;
			sendNotification(Notifications.PREFERENCES_CHANGE, preferences);
		}

		TemplateMappingPreferences templatesMappingPreferences = closeTemplateMapping();
		ContentAssistPreferences contentAssistPreferences = closeContentAssist();
		InsertsPreferences insertsPreferences = closeInserts();
		SymbolsPreferences symbolsPreferences = closeSymbols();
		LookupsPreferences lookupsPreferences = closeLookups();
		
		if (templatesMappingPreferences != null) sendNotification(Notifications.PREFERENCES_CHANGE_TEMPLATE_MAPPING, templatesMappingPreferences);
		if (contentAssistPreferences != null) sendNotification(Notifications.PREFERENCES_CHANGE_CONTENT_ASSIST, contentAssistPreferences);
		if (insertsPreferences != null) sendNotification(Notifications.PREFERENCES_CHANGE_INSERTS, insertsPreferences);
		if (symbolsPreferences != null) sendNotification(Notifications.PREFERENCES_CHANGE_SYMBOLS, symbolsPreferences);
		if (lookupsPreferences != null) sendNotification(Notifications.PREFERENCES_CHANGE_LOOKUPS, lookupsPreferences);
		
		// notify preference changes:
		sendNotification(Notifications.MENU_PREFERENCES_CONFIRMED);
		
		// save preferences:
		sendNotification(Notifications.PREFERENCES_SAVE);
		
		// restart GUI
		boolean restart = false;
		if (!guiLangCode.equals(MinorityTranslateModel.preferences().display().getGUILangCode())) restart = true;
		if (!fontSize.equals(MinorityTranslateModel.preferences().display().getFontSize())) restart = true;
		
		if (restart) sendNotification(Notifications.GUI_RESTART);
		
		
     }

	@Override
    public void decline()
     {
		// close pages
    	closeInserts();
    	closeContentAssist();
		closeSymbols();
		closeLookups();
     }

	
	// HELPERS
	public String[] collectGUILanguages()
	 {
		String[] guiLangNames = new String[guiLangCodes.length];
		for (int i = 0; i < guiLangNames.length; i++) {
			String langName = MinorityTranslateModel.wikis().getLangName(guiLangCodes[i]);
			if (langName == null) langName = guiLangCodes[i];
			guiLangNames[i] = langName;
		}
		
		return guiLangNames;
	 }
	
	
}
