package ee.translate.keeleleek.mtapplication.model.processing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexReplace extends RegexCollect {

	private String replace = null;

	
	// INIT
	public RegexReplace(String rgxFind, String replace)
	 {
		super(rgxFind, new ArrayList<>()); // TODO
		this.replace = replace;
	 }
	

	// REPLACE
	public String replace(String text, HashMap<String, String> variableMap)
	 {
		// collect variables
		collect(text, variableMap);
		
		// replace
		text = Pattern.compile(rgxFind, Pattern.MULTILINE | Pattern.DOTALL).matcher(text).replaceAll(Matcher.quoteReplacement(replace));
		
		return text;
	 }
	
	
}
