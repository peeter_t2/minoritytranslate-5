package ee.translate.keeleleek.mtapplication.view.pages;

import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.model.preferences.ContentPreferences;


public abstract class ContentPageMediator extends Mediator implements PreferencesPage<ContentPreferences> {

	public final static String NAME = "{C7C32064-CDD8-499F-86C6-13C0A2F5B23B}";
	
	
	// INIT
	public ContentPageMediator() {
		super(NAME, null);
	}

	@Override
	public void onRegister() {
		
	}

	
}
