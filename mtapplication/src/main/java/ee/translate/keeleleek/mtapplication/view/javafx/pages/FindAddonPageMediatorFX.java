package ee.translate.keeleleek.mtapplication.view.javafx.pages;

import ee.translate.keeleleek.mtapplication.common.requests.FindReplaceRequest;
import ee.translate.keeleleek.mtapplication.common.requests.FindReplaceRequest.Type;
import ee.translate.keeleleek.mtapplication.view.pages.FindAddonPageMediator;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;

public class FindAddonPageMediatorFX extends FindAddonPageMediator {
	

	@FXML
	private TextField findEdit;
	@FXML
	private TextField replaceEdit;
	
	@FXML
	private CheckBox wrapSelect;
	@FXML
	private CheckBox caseSensitiveSelect;
	@FXML
	private CheckBox wholeWordSelect;
	
	@FXML
	private Button findNextButton;
	@FXML
	private Button findPreviousButton;
	@FXML
	private Button replaceButton;
	@FXML
	private Button replaceAllButton;
	
	
	// INIT
	@Override
	public void onRegister()
	 {
		super.onRegister();
		
	 }

	@FXML
	protected void initialize()
	 {
		findEdit.textProperty().addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable observable) {
				boolean findEmpty = findEdit.getText().isEmpty();
				findNextButton.setDisable(findEmpty);
				findPreviousButton.setDisable(findEmpty);
				replaceButton.setDisable(findEmpty);
				replaceAllButton.setDisable(findEmpty);
			}
		});
		
	 }

	
	// INHERIT (PAGE)
	@Override
	public void open()
	 {
		
	 }
	
	@Override
	public void close()
	 {
		
	 }

	
	// EVENTS
	@FXML
	private void onFindNextClick()
	 {
		FindReplaceRequest request = createRequest(Type.FIND_NEXT);
		onFind(request);
	 }

	@FXML
	private void onFindPreviousClick()
	 {
		FindReplaceRequest request = createRequest(Type.FIND_PREVIOUS);
		onFind(request);
	 }
	
	@FXML
	private void onReplaceClick()
	 {
		onFind(createRequest(Type.REPLACE));
	 }
	
	@FXML
	private void onReplaceAllClick()
	 {
		onFind(createRequest(Type.REPLACE_ALL));
	 }
	
	
	// HELPERS
	private FindReplaceRequest createRequest(Type type) {
		return new FindReplaceRequest(type, findEdit.getText(), replaceEdit.getText(), wrapSelect.isSelected(), caseSensitiveSelect.isSelected(), wholeWordSelect.isSelected());
	}
	
}
