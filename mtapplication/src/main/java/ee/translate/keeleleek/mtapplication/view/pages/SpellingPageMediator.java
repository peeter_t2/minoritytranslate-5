package ee.translate.keeleleek.mtapplication.view.pages;

import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.model.preferences.SpellingPreferences;


public abstract class SpellingPageMediator extends Mediator implements PreferencesPage<SpellingPreferences> {

	public final static String NAME = "{3C1DD10A-C502-4C3B-A3F3-FBEFCAE9C642}";
	
	
	// INIT
	public SpellingPageMediator() {
		super(NAME, null);
	}

	@Override
	public void onRegister() {
		
	}

	
}
