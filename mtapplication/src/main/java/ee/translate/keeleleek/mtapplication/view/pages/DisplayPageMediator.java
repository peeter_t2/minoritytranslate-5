package ee.translate.keeleleek.mtapplication.view.pages;

import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.model.preferences.DisplayPreferences;


public abstract class DisplayPageMediator extends Mediator implements PreferencesPage<DisplayPreferences> {

	public final static String NAME = "{548CDE71-E514-4345-BAAE-4DC27E207570}";
	
	
	// INIT
	public DisplayPageMediator() {
		super(NAME, null);
	}

	@Override
	public void onRegister() {
		
	}

	
}
