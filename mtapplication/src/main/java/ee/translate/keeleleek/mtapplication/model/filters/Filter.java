package ee.translate.keeleleek.mtapplication.model.filters;

public interface Filter {

	/**
	 * Filter name.
	 * 
	 * @return filter name
	 */
	String getName();
	
	/**
	 * Filters text.
	 * 
	 * @param text text
	 * @return filtered text
	 */
	String filter(String text);
	
}
