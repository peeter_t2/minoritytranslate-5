package ee.translate.keeleleek.mtapplication.model.content;

import java.util.ArrayList;
import java.util.Collection;

public class WikiRequest {

	private WikiReference original;
	private String qid;
	private Collection<WikiReference> requests;
	private QidMeta qidMeta;
	
	
	// INIT
	public WikiRequest(WikiReference original, String qid, Collection<WikiReference> requests, QidMeta qidMeta) throws NullPointerException
	 {
		if (qid == null) throw new NullPointerException("qid cannot be null");
		if (original == null) throw new NullPointerException("original request cannot be null");
		if (requests == null) requests = new ArrayList<WikiReference>();
		
		this.original = original;
		this.qid = qid;
		this.requests = requests;
		this.qidMeta = qidMeta;
	 }
	
	
	// VALUES
	public String getQid() {
		return qid;
	}
	
	public WikiReference getOriginal() {
		return original;
	}
	
	public Collection<WikiReference> getRequests() {
		return requests;
	}
	
	public QidMeta getQidMeta() {
		return qidMeta;
	}
	
	
	// WORKINGS
	@Override
	public String toString() {
		return "{qid=" + qid + " original=" + original.toString() + " requests=" + requests.toString() + "}";
	}
	
	@Override
	public boolean equals(Object obj)
	 {
		if (obj instanceof WikiRequest) {
			WikiRequest cobj = (WikiRequest) obj;
			return qid.equals(cobj.qid);
		} else return false;
	 }
	
	@Override
	public int hashCode() {
		return qid.hashCode();
	}
	
	
}
