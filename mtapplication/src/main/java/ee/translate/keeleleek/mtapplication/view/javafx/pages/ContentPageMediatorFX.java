package ee.translate.keeleleek.mtapplication.view.javafx.pages;

import ee.translate.keeleleek.mtapplication.model.preferences.ContentPreferences;
import ee.translate.keeleleek.mtapplication.view.listeners.IntegerKeyEventHandler;
import ee.translate.keeleleek.mtapplication.view.pages.ContentPageMediator;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;


public class ContentPageMediatorFX extends ContentPageMediator {

	@FXML
	private TextField categoryDepthEdit;
	
	
	/* ******************
	 *                  *
	 *  Initialization  *
	 *                  *
	 ****************** */
	@Override
	public void onRegister()
	 {
		super.onRegister();
		
	 }

	@FXML
	protected void initialize()
	 {
		categoryDepthEdit.addEventFilter(KeyEvent.KEY_TYPED, new IntegerKeyEventHandler()); // only int
	 }

	
	
	/* ******************
	 *                  *
	 *       Page       *
	 *                  *
	 ****************** */
	@Override
	public void open(ContentPreferences preferences)
	 {
		categoryDepthEdit.setText(preferences.getCategoryDepth().toString());
	 }
	
	@Override
	public ContentPreferences close()
	 {
		Integer categoryDepth = 0;
		try {
			categoryDepth = Integer.parseInt(categoryDepthEdit.getText());
		} catch (NumberFormatException e) { }
		
		return new ContentPreferences(categoryDepth);
	 }

	
}
