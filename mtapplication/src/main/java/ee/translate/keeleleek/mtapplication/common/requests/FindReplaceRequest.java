package ee.translate.keeleleek.mtapplication.common.requests;

public class FindReplaceRequest {

	private Type type;
	private String find;
	private String replace = null;
	private boolean wrap;
	private boolean caseSensitive;
	private boolean wholeWord;
	
	
	public FindReplaceRequest(Type type, String find, String replace, boolean wrap, boolean caseSencitive, boolean wholeWord) {
		this.type = type;
		this.find = find;
		this.replace = replace;
		this.wrap = wrap;
		this.caseSensitive = caseSencitive;
		this.wholeWord = wholeWord;
	}

	public Type getType() {
		return type;
	}
	
	public void setType(Type type) {
		this.type = type;
	}
	
	public String getFind() {
		return find;
	}
	
	public void setFind(String find) {
		this.find = find;
	}
	
	public String getReplace() {
		return replace;
	}
	
	public void setReplace(String replace) {
		this.replace = replace;
	}

	public boolean isWrap() {
		return wrap;
	}

	public void setWrap(boolean wrap) {
		this.wrap = wrap;
	}

	public boolean isCaseSensitive() {
		return caseSensitive;
	}

	public void setCaseSensitive(boolean caseSencitive) {
		this.caseSensitive = caseSencitive;
	}

	public boolean isWholeWord() {
		return wholeWord;
	}

	public void setWholeWord(boolean wholeWord) {
		this.wholeWord = wholeWord;
	}

	public enum Type {
		FIND_NEXT,
		REPLACE,
		FIND_PREVIOUS,
		REPLACE_ALL
	}
	
	
}
