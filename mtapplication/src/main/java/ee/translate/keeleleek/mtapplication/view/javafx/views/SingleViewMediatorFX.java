package ee.translate.keeleleek.mtapplication.view.javafx.views;

import java.util.ArrayList;
import java.util.List;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.view.elements.EditorMediator.EditorPosition;
import javafx.fxml.FXML;

public class SingleViewMediatorFX extends ViewMediatorFX {

	
	/* ******************
	 *                  *
	 *  Initialization  *
	 *                  *
	 ****************** */
	public SingleViewMediatorFX(ViewMode viewMode)
	 {
		super(viewMode);
	 }
	
	@FXML
	private void initialize()
	 {
		//super.initialize();
	 }
	
	
	
	/* ******************
	 *                  *
	 *     Refresh      *
	 *                  *
	 ****************** */

	
	
	
	/* ******************
	 *                  *
	 *    Languages     *
	 *                  *
	 ****************** */
	@Override
	public List<String> langCodesFor(EditorPosition pos)
	 {
		List<String> langCodes;
		if (pos == EditorPosition.DESTINATION) langCodes = MinorityTranslateModel.preferences().languages().filterActiveLangCodes();
		else langCodes = new ArrayList<>();
		return langCodes;
	 }
	
	
	
	/* ******************
	 *                  *
	 *      Events      *
	 *                  *
	 ****************** */
	@Override
	public String findSelectedSrcLangCode() {
		return null;
	}
	
}
