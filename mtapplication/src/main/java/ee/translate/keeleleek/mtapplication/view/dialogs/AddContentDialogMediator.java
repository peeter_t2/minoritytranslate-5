package ee.translate.keeleleek.mtapplication.view.dialogs;

import java.util.List;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.common.requests.ContentRequest;


public abstract class AddContentDialogMediator extends Mediator implements ConfirmDeclineDialog {

	public final static String NAME = "{E494535C-0592-4349-B2F2-02386EA970D1}";

	
	// INIT
	public AddContentDialogMediator() {
		super(NAME, null);
	}

	@Override
	public void onRegister()
	 {
		
	 }
	
	
	// STATE:
	@Override
	public String[] listNotificationInterests() {
		return new String[]
		 {
		 };
	}
	
	@Override
	public void handleNotification(INotification notification)
	 {
		
	 }

	
	// INHERIT (CONTENT)
	public abstract List<ContentRequest> collectContentRequests();
	
	// INHERIT (CONFIRM DECLINE DIALOG)
	@Override
	public void prepare()
	 {
		
	 }
	
	@Override
	public void focus()
	 {
		
	 }
	
	@Override
	public void confirm()
	 {
		
	 }
	
	@Override
	public void decline()
	 {
		
	 }
	

	
}
