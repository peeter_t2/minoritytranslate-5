package ee.translate.keeleleek.mtapplication.view.javafx.elements;

import java.util.List;

import ee.translate.keeleleek.mtapplication.model.processing.processers.VariableProcesser;
import ee.translate.keeleleek.mtapplication.view.elements.SnippetsPaneMediator;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Pagination;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination.ModifierValue;


public class SnippetsPaneMediatorFX extends SnippetsPaneMediator {

	@FXML
	private TextArea snippetEdit;
	@FXML
	private Label keyCodeLabel;
	@FXML
	private Pagination keySelection;
	@FXML
	private MenuButton variablesButton;
	
	
	// INIT
	@FXML
	private void initialize()
	 {
		variablesButton.getItems().clear();
		List<String> variables = VariableProcesser.collectAllVariables();
		for (String variable : variables) {
			MenuItem item = new MenuItem(variable);
			item.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					if (snippetEdit.isFocused()) snippetEdit.replaceSelection(((MenuItem) event.getSource()).getText());
				}
			});
			variablesButton.getItems().add(item);
		}
		
		snippetEdit.focusedProperty().addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable observable) {
				variablesButton.setDisable(!snippetEdit.isFocused());
			}
		});
	 }
	
	@Override
	public void onRegister()
	 {
		super.onRegister();
		
		snippetEdit.textProperty().addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable arg0) {
				if (feedback) return;
				onSnippetChanged(keySelection.getCurrentPageIndex(), snippetEdit.getText());
			}
		});
		
		keyCodeLabel.setText(getCodeLabelText());
		
		keySelection.currentPageIndexProperty().addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable arg0) {
				onSelectionChanged();
			}
		});
	 }
	
	
	// INHERIT
	@Override
	protected int getKeyIndex()
	 {
		return keySelection.getCurrentPageIndex();
	 }
	
	@Override
	protected String getSnippet() {
		return snippetEdit.getText();
	}
	
	@Override
	protected void setSnippet(String snippet) {
		snippetEdit.setText(snippet);
	}
	
	
	// EVENTS
	@FXML
	private void onSelectionChanged()
	 {
		super.onSelectedSnippetChanged(keySelection.getCurrentPageIndex());
		
		keyCodeLabel.setText(getCodeLabelText());
	 }
	
	
	// HELPERS
	private String getCodeLabelText()
	 {
		int n = keySelection.getCurrentPageIndex() + 1;
		KeyCodeCombination comb = new KeyCodeCombination(KeyCode.getKeyCode("" + n), ModifierValue.ANY, ModifierValue.DOWN, ModifierValue.ANY, ModifierValue.ANY, ModifierValue.ANY);
		return comb.getDisplayText();
	 }
	
}
