package ee.translate.keeleleek.mtapplication.model.session;

import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.puremvc.java.multicore.patterns.facade.Facade;
import org.puremvc.java.multicore.patterns.proxy.Proxy;

import ee.translate.keeleleek.mtapplication.MinorityTranslate;
import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.model.content.ContentProxy;
import ee.translate.keeleleek.mtapplication.model.keeleleek.UsageInfoProxy;


public class SessionProxy extends Proxy {
	
	public final static String NAME = "{D7B1DF89-F6C6-4E9B-8348-7B0B562CAFEB}";

	public final static Path DEFFAULT_SESSIONS_PATH_PREFERED = Paths.get(MinorityTranslate.ROOT_PATH);
	public final static Path DEFFAULT_SESSIONS_PATH = Paths.get("").toAbsolutePath();
	
	private String name = null;
	private URI workingPath = null;
	private String notes = "";
	private String tag = "";
	private boolean uploadEnabled = true;
	
	transient private Path sessionPath = null;
	
	private UsageInfoProxy usage = null;
	private ContentProxy content = null;
	
	
	// INIT
	public SessionProxy() {
		super(NAME);
	}
	
	@Override
	public void onRegister()
	 {
		Facade facade = getFacade();
		
		if (usage == null) usage = new UsageInfoProxy();
		facade.registerProxy(usage);

		if (content == null) content = new ContentProxy();
		facade.registerProxy(content);

		facade.sendNotification(Notifications.SESSION_LOADED);
	 }

	@Override
	public void onRemove()
	 {
		Facade facade = getFacade();
		
		facade.removeProxy(usage.getProxyName());
		facade.removeProxy(content.getProxyName());
	 }
	
	
	// NAME
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	
	// LOCATION
	/**
	 * Gets the current session path.
	 * 
	 * @return current session path; null if none
	 */
	public Path getSessionPath() {
		return sessionPath;
	}
	
	/**
	 * Sets the current session path.
	 * 
	 * @param path current session path; null if none
	 */
	public void setSessionPath(Path path) {
		this.sessionPath = path;
	}
	
	
	// PROXIES
	/**
	 * Gets the usage.
	 * 
	 * @return the usage
	 */
	public UsageInfoProxy getUsage() {
		return usage;
	}

	/**
	 * Sets the usage.
	 * 
	 * @param usage the usage to set
	 */
	public void setUsage(UsageInfoProxy usage) {
		this.usage = usage;
	}

	
	// VALUES
	/**
	 * Gets the working path.
	 * 
	 * @return working path; null if none
	 */
	public Path getWorkingPath() {
		Path sessionPath = null;
		if (this.workingPath != null) sessionPath = Paths.get(this.workingPath);
		if (sessionPath != null && Files.isDirectory(sessionPath)) return sessionPath;
		if (Files.isDirectory(DEFFAULT_SESSIONS_PATH_PREFERED)) return DEFFAULT_SESSIONS_PATH_PREFERED;
		return DEFFAULT_SESSIONS_PATH;
	}

	/**
	 * Sets the working path.
	 * 
	 * @param workingPath the working path; null if none
	 */
	public void setWorkingPath(Path workingPath) {
		this.workingPath = workingPath.toUri();
	}

	
	/**
	 * Gets notes.
	 * 
	 * @return notes
	 */
	public String getNotes() {
		return notes;
	}
	
	/**
	 * Sets notes.
	 * 
	 * @param notes notes
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	/**
	 * Changes the tag.
	 * 
	 * @param tag tag
	 */
	public void changeTag(String tag) {
		this.tag = tag;
		sendNotification(Notifications.SESSION_TAG_CHANGED, this.tag);
	}
	
	/**
	 * Get the tag.
	 * 
	 * @return tag
	 */
	public String getTag() {
		return tag;
	}
	
	
	public boolean isUploadEnabled() {
		return uploadEnabled;
	}
	
	public void setUploadEnabled(boolean uploadEnabled) {
		this.uploadEnabled = uploadEnabled;
	}
	
	
}
