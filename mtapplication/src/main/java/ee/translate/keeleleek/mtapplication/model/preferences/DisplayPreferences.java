package ee.translate.keeleleek.mtapplication.model.preferences;

import ee.translate.keeleleek.mtapplication.model.filters.FilesFilter;
import ee.translate.keeleleek.mtapplication.model.filters.Filter;
import ee.translate.keeleleek.mtapplication.model.filters.IntroductionFilter;
import ee.translate.keeleleek.mtapplication.model.filters.ReferencesFilter;
import ee.translate.keeleleek.mtapplication.model.filters.TemplatesFilter;


public class DisplayPreferences {

	public final static String[] FONT_SIZES = new String[]{"150%", "140%", "130%", "120%", "110%", "100%", "90%", "80%", "70%", "60%", "50%"};
	
	private Boolean templatesFilter;
	private Boolean filesFilter;
	private Boolean introductionFilter;
	private Boolean referencesFilter;
	private String guiLangCode = "en";
	private String fontSize = "100%";
	
	
	// INIT
	public DisplayPreferences() {
		// TODO Auto-generated constructor stub
	}
	
	public DisplayPreferences(Boolean templatesFilter, Boolean filesFilter, Boolean introductionFilter, Boolean referencesFilter, String guiLangCode, String fontSize) // TODO
	 {
		this.templatesFilter = templatesFilter;
		this.filesFilter = filesFilter;
		this.introductionFilter = introductionFilter;
		this.referencesFilter = referencesFilter;
		this.guiLangCode = guiLangCode;
		this.fontSize = fontSize;
	 }

	public DisplayPreferences(DisplayPreferences preferences)
	 {
		this.templatesFilter = preferences.templatesFilter;
		this.filesFilter = preferences.filesFilter;
		this.introductionFilter = preferences.introductionFilter;
		this.referencesFilter = preferences.referencesFilter;
		this.guiLangCode = preferences.guiLangCode;
		this.fontSize = preferences.fontSize;
	 }

	public static DisplayPreferences create()
	 {
		DisplayPreferences preferences = new DisplayPreferences();
		
		preferences.templatesFilter = false;
		preferences.filesFilter = false;
		preferences.introductionFilter = false;
		preferences.referencesFilter = false;
		preferences.guiLangCode = "en";
		
		return preferences;
	 }
	

	// PREFERENCES
	public Boolean isTemplatesFilter() {
		return templatesFilter;
	}

	public void setTemplatesFilter(Boolean templatesFilter) {
		this.templatesFilter = templatesFilter;
	}
	
	public Boolean isFilesFilter() {
		return filesFilter;
	}
	
	public void setFilesFilter(Boolean filesFilter) {
		this.filesFilter = filesFilter;
	}

	public Boolean isIntroductionFilter() {
		return introductionFilter;
	}
	
	public void setIntroductionFilter(Boolean introductionFilter) {
		this.introductionFilter = introductionFilter;
	}
	
	public Boolean isReferencesFilter() {
		return referencesFilter;
	}

	public void setReferencesFilter(Boolean referencesFilter) {
		this.referencesFilter = referencesFilter;
	}
	
	public String getGUILangCode() {
		return guiLangCode;
	}

	public void setGuiLangCode(String guiLangCode) {
		this.guiLangCode = guiLangCode;
	}
	
	public String getFontSize() {
		return fontSize;
	}
	
	public void setFontSize(String interfaceFontSize) {
		this.fontSize = interfaceFontSize;
	}
	
	
	// FILTERS
	public Filter[] collectActiveFilters()
	 {
		int size = 0;
		
		if (templatesFilter) size++;
		if (filesFilter) size++;
		if (introductionFilter) size++;
		if (referencesFilter) size++;
		
		Filter[] result = new Filter[size];
		
		int i = 0;

		if (templatesFilter) {
			result[i] = new TemplatesFilter();
			i++;
		}

		if (filesFilter) {
			result[i] = new FilesFilter();
			i++;
		}

		if (introductionFilter) {
			result[i] = new IntroductionFilter();
			i++;
		}

		if (referencesFilter) {
			result[i] = new ReferencesFilter();
			i++;
		}
		
		return result;
	 }

	

	// UTILITY
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((filesFilter == null) ? 0 : filesFilter.hashCode());
		result = prime * result + ((fontSize == null) ? 0 : fontSize.hashCode());
		result = prime * result + ((guiLangCode == null) ? 0 : guiLangCode.hashCode());
		result = prime * result + ((introductionFilter == null) ? 0 : introductionFilter.hashCode());
		result = prime * result + ((referencesFilter == null) ? 0 : referencesFilter.hashCode());
		result = prime * result + ((templatesFilter == null) ? 0 : templatesFilter.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DisplayPreferences other = (DisplayPreferences) obj;
		if (filesFilter == null) {
			if (other.filesFilter != null)
				return false;
		} else if (!filesFilter.equals(other.filesFilter))
			return false;
		if (fontSize == null) {
			if (other.fontSize != null)
				return false;
		} else if (!fontSize.equals(other.fontSize))
			return false;
		if (guiLangCode == null) {
			if (other.guiLangCode != null)
				return false;
		} else if (!guiLangCode.equals(other.guiLangCode))
			return false;
		if (introductionFilter == null) {
			if (other.introductionFilter != null)
				return false;
		} else if (!introductionFilter.equals(other.introductionFilter))
			return false;
		if (referencesFilter == null) {
			if (other.referencesFilter != null)
				return false;
		} else if (!referencesFilter.equals(other.referencesFilter))
			return false;
		if (templatesFilter == null) {
			if (other.templatesFilter != null)
				return false;
		} else if (!templatesFilter.equals(other.templatesFilter))
			return false;
		return true;
	}

	
}
