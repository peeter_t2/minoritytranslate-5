package ee.translate.keeleleek.mtapplication.model.lists;

public class QueryCategoryList implements SuggestionList {
	
	public final static String GROUP_CATEGORY = "Category";
	
	private String langCode;
	private String title;
	
	
	public QueryCategoryList(String langCode, String title) {
		this.langCode = langCode;
		this.title = title;
	}
	
	
	public String getLangCode() {
		return langCode;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getName() {
		return langCode + ":" + title;
	}
	
	@Override
	public String getGroup() {
		return GROUP_CATEGORY;
	}
	
	
}
