package ee.translate.keeleleek.mtpluginframework.chopup;

public class TagEndElement extends ArticleElement {

	private String name = "";
	
	public TagEndElement() {
		super(Type.TAG_END);
	}
	
	@Override
	public void init(String text) {
		this.name = text;
	}
	
	public String getName() {
		return name;
	}
	
	@Override
	protected String asCode() {
		return "</" + name + ">";
	}
	
}
