package ee.translate.keeleleek.mtpluginframework.chopup;

import java.util.regex.Pattern;

public class WikilinkElement extends ArticleElement {

	private String namespace = "";
	private String link = "";
	private String[] args = new String[0];
	private String attachment = null;
	
	private boolean linkTouched = false;
	
	
	// INIT
	public WikilinkElement() {
		super(Type.WIKILINK);
	}
	
	@Override
	public void init(String srcText)
	 {
		String[] split = srcText.split(Pattern.quote("|"));
		
		// link
		link = split[0];
		
		// extract namespace
		int i = this.link.indexOf(':');
		if (i != -1) {
			namespace = link.substring(0, i);
			link = link.substring(i + 1);
		}
		
		// arguments
		int argsCount = split.length - 1;
		
		if (namespace.isEmpty() && argsCount == 0) argsCount = 1; // regular links must have at least one argument
		
		args = new String[argsCount];
		for (int j = 1; j < split.length; j++) {
			args[j - 1] = split[j];
		}
		
		if (namespace.isEmpty()) {
			if (args[args.length - 1] == null) args[args.length - 1] = link; // regular link name
			if (attachment != null) { // swallow attachment
				args[args.length - 1] += attachment;
				attachment = null;
			}
		}
	 }
	
	@Override
	protected String asCode()
	 {
		StringBuilder codeBuilder = new StringBuilder();
		codeBuilder.append("[[");
		
		if (!namespace.isEmpty()) codeBuilder.append(namespace + ':');
		
		codeBuilder.append(link);
		
		for (int i = 0; i < args.length; i++) {
			codeBuilder.append("|" + args[i]);
		}
		
		codeBuilder.append("]]");
		
		if (attachment != null) codeBuilder.append(attachment);
		
		return  codeBuilder.toString();
	 }

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}
	
	public String getLink()
	 {
		return link;
	 }
	
	public void setLink(String link)
	 {
		this.link = link;
		this.linkTouched = true;
	 }
	
	public boolean isLinkTouched() {
		return linkTouched;
	}
	
	public String getText() {
		if (args.length == 0) return null;
		return args[args.length - 1];
	}
	
	public void setText(String text) {
		if (args.length == 0) return;
		args[args.length - 1] = text;
	}

	public void attach(String attachment) {
		this.attachment = attachment;
	}
	
	
}
