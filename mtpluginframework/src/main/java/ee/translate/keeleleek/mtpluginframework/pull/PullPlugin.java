package ee.translate.keeleleek.mtpluginframework.pull;

import ee.translate.keeleleek.mtpluginframework.MinorityTranslatePlugin;
import ee.translate.keeleleek.mtpluginframework.PluginVersion;
import ee.translate.keeleleek.mtpluginframework.chopup.RootElement;

/**
 * Interface for pull plugins.
 *
 */
public interface PullPlugin extends MinorityTranslatePlugin {

	/**
	 * Gets pull plugin version.
	 * 
	 * @return pull plugin version
	 */
	public PluginVersion getPullVersion();

	/**
	 * Performs a pull.
	 * 
	 * @param srcLangCode source language code
	 * @param dstLangCode destination language code
	 * @param srcTitle source title
	 * @param srcRoot chopped up source text to modify
	 * @param dstTitle destination title
	 * @param dstText destination text
	 * @param progress progress callback
	 * @return true on success
	 * @throws Exception when the pull fails
	 */
	public boolean pull(String srcLangCode, String dstLangCode, String srcTitle, RootElement srcRoot, String dstTitle, String dstText, PullCallback progress) throws Exception;
	
	
}
