package ee.translate.keeleleek.mtpluginframework;

import java.nio.file.Path;

import ee.translate.keeleleek.mtpluginframework.pull.PullPlugin;
import ee.translate.keeleleek.mtpluginframework.spellcheck.SpellerPlugin;

/**
 * Application callback interface.
 */
public interface MinorityTranslatePlugins {

	/**
	 * Finds a speller plugin with the given name.
	 * 
	 * @param pluginName plugin name
	 * @return speller plugin with the given name, null if none
	 */
	public SpellerPlugin findSpellerPlugin(String pluginName);

	/**
	 * Finds a pull plugin with the given name.
	 * 
	 * @param pluginName plugin name
	 * @return pull plugin with the given name, null if none
	 */
	public PullPlugin findPullPlugin(String pluginName);

	/**
	 * Gets message string.
	 * 
	 * @param key message key
	 * @return message for the given key
	 */
	public String getStringMessage(String key);

	/**
	 * Gets root path.
	 */
	public Path getRootPath();

	/**
	 * Gets plugins path.
	 */
	public Path getPluginsPath();
	
	
}
